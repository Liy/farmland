# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120304214849) do

  create_table "assignments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "assignments", ["role_id"], :name => "index_assignments_on_role_id"
  add_index "assignments", ["user_id", "role_id"], :name => "index_assignments_on_user_id_and_role_id", :unique => true
  add_index "assignments", ["user_id"], :name => "index_assignments_on_user_id"

  create_table "crops", :force => true do |t|
    t.string   "category",                       :null => false
    t.string   "variety"
    t.string   "quality"
    t.date     "growth_start",                   :null => false
    t.date     "growth_end",                     :null => false
    t.integer  "pesticide_residue_benchmark_id"
    t.string   "image"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "farmer_addresses", :force => true do |t|
    t.string   "label"
    t.string   "country_region"
    t.string   "province"
    t.string   "city"
    t.string   "detail"
    t.string   "postcode"
    t.integer  "farmer_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "farmer_addresses", ["city"], :name => "index_farmer_addresses_on_city"
  add_index "farmer_addresses", ["country_region"], :name => "index_farmer_addresses_on_country_region"
  add_index "farmer_addresses", ["detail"], :name => "index_farmer_addresses_on_detail"
  add_index "farmer_addresses", ["postcode"], :name => "index_farmer_addresses_on_postcode"
  add_index "farmer_addresses", ["province"], :name => "index_farmer_addresses_on_province"

  create_table "farmer_emails", :force => true do |t|
    t.string   "label"
    t.string   "value"
    t.integer  "farmer_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "farmer_telecoms", :force => true do |t|
    t.string   "label"
    t.string   "value"
    t.integer  "farmer_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "farmers", :force => true do |t|
    t.string   "name",       :null => false
    t.float    "arable"
    t.float    "cultivated"
    t.string   "image"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "farmers", ["name"], :name => "index_farmers_on_name"

  create_table "fertilizer_applications", :force => true do |t|
    t.float    "amount"
    t.date     "date"
    t.integer  "grow_id"
    t.integer  "fertilizer_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "fertilizer_applications", ["fertilizer_id"], :name => "index_fertilizer_applications_on_fertilizer_id"
  add_index "fertilizer_applications", ["grow_id"], :name => "index_fertilizer_applications_on_grow_id"

  create_table "fertilizers", :force => true do |t|
    t.string   "name"
    t.string   "category",   :null => false
    t.float    "n"
    t.float    "p"
    t.float    "k"
    t.string   "others"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "grows", :force => true do |t|
    t.float    "sown_area"
    t.date     "sowing_date"
    t.integer  "crop_id"
    t.integer  "farmer_id"
    t.string   "note"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "guidances", :force => true do |t|
    t.integer  "user_id"
    t.integer  "farmer_id"
    t.string   "note"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "guidances", ["farmer_id"], :name => "index_guidances_on_farmer_id"
  add_index "guidances", ["user_id", "farmer_id"], :name => "index_guidances_on_user_id_and_farmer_id", :unique => true
  add_index "guidances", ["user_id"], :name => "index_guidances_on_user_id"

  create_table "harms", :force => true do |t|
    t.string   "name",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "harvest_infos", :force => true do |t|
    t.date     "date"
    t.float    "yield"
    t.float    "price"
    t.integer  "grow_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "image_taggings", :force => true do |t|
    t.integer  "image_id"
    t.integer  "image_tag_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "image_taggings", ["image_id", "image_tag_id"], :name => "index_image_taggings_on_image_id_and_image_tag_id", :unique => true
  add_index "image_taggings", ["image_id"], :name => "index_image_taggings_on_image_id"
  add_index "image_taggings", ["image_tag_id"], :name => "index_image_taggings_on_image_tag_id"

  create_table "image_tags", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "image_tags", ["name"], :name => "index_image_tags_on_name"

  create_table "images", :force => true do |t|
    t.string   "filename"
    t.integer  "user_id"
    t.string   "serial_number"
    t.string   "lens_model"
    t.datetime "date_time_original"
    t.string   "orientation"
    t.string   "info"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "images", ["filename"], :name => "index_images_on_filename"
  add_index "images", ["user_id"], :name => "index_images_on_user_id"

  create_table "pesticide_applications", :force => true do |t|
    t.date     "date"
    t.float    "amount"
    t.integer  "grow_id"
    t.integer  "pesticide_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "pesticides", :force => true do |t|
    t.string   "category"
    t.string   "name"
    t.string   "image"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "pesticides", ["category"], :name => "index_pesticides_on_category"

  create_table "roles", :force => true do |t|
    t.string "name"
  end

  create_table "sufferings", :force => true do |t|
    t.integer  "grow_id"
    t.integer  "harm_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sufferings", ["grow_id"], :name => "index_sufferings_on_grow_id"
  add_index "sufferings", ["harm_id"], :name => "index_sufferings_on_harm_id"

  create_table "user_addresses", :force => true do |t|
    t.string   "label"
    t.string   "value"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "user_addresses", ["user_id"], :name => "index_user_addresses_on_user_id"

  create_table "user_emails", :force => true do |t|
    t.string   "label"
    t.string   "value"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "user_emails", ["user_id"], :name => "index_user_emails_on_user_id"

  create_table "user_telecoms", :force => true do |t|
    t.string   "label"
    t.string   "value"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "user_telecoms", ["user_id"], :name => "index_user_telecoms_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "username",             :null => false
    t.string   "email",                :null => false
    t.string   "password_digest",      :null => false
    t.string   "name",                 :null => false
    t.string   "mobile"
    t.string   "avatar"
    t.string   "nickname"
    t.string   "activation_token"
    t.string   "password_reset_token"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "users", ["activation_token"], :name => "index_users_on_activation_token"
  add_index "users", ["email"], :name => "index_users_on_email"
  add_index "users", ["name"], :name => "index_users_on_name"
  add_index "users", ["nickname"], :name => "index_users_on_nickname"
  add_index "users", ["password_reset_token"], :name => "index_users_on_password_reset_token"
  add_index "users", ["username"], :name => "index_users_on_username"

  create_table "versions", :force => true do |t|
    t.string   "item_type",  :null => false
    t.integer  "item_id",    :null => false
    t.string   "event",      :null => false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], :name => "index_versions_on_item_type_and_item_id"

end
