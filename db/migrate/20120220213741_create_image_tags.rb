class CreateImageTags < ActiveRecord::Migration
  def change
    create_table :image_tags do |t|
      t.string :name

      t.timestamps
    end
    add_index :image_tags, :name
  end
end
