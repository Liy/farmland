class CreateFarmerAddresses < ActiveRecord::Migration
  def change
    create_table :farmer_addresses do |t|
      t.string :label
      t.string :country_region
      t.string :province
      t.string :city
      t.string :detail
      t.string :postcode
      t.integer :farmer_id

      t.timestamps
    end
    
    # I guess they will be quite common search terms.
    add_index :farmer_addresses, :country_region
    add_index :farmer_addresses, :city
    add_index :farmer_addresses, :province
    add_index :farmer_addresses, :detail
    add_index :farmer_addresses, :postcode
    
  end
end
