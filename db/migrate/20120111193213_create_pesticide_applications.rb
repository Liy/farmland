class CreatePesticideApplications < ActiveRecord::Migration
  def change
    create_table :pesticide_applications do |t|
      t.date :date
      t.float :amount
      t.integer :grow_id
      t.integer :pesticide_id

      t.timestamps
    end
  end
end
