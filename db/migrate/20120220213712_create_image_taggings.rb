class CreateImageTaggings < ActiveRecord::Migration
  def change
    create_table :image_taggings do |t|
      t.integer :image_id
      t.integer :image_tag_id

      t.timestamps
    end
    
    add_index :image_taggings, :image_id
    add_index :image_taggings, :image_tag_id
    # same image and tag combination should only exist once
    add_index :image_taggings, [:image_id, :image_tag_id], :unique => true
    
  end
end
