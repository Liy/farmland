class CreateFarmerTelecoms < ActiveRecord::Migration
  def change
    create_table :farmer_telecoms do |t|
      t.string :label
      t.string :value
      t.integer :farmer_id

      t.timestamps
    end
  end
end
