class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :filename
      t.integer :user_id
      
      t.string :serial_number
      t.string :lens_model
      t.datetime :date_time_original
      t.string :orientation
      
      
      t.string :info

      t.timestamps
    end
    add_index :images, :filename
    add_index :images, :user_id
  end
end
