class CreateFertilizers < ActiveRecord::Migration
  def change
    create_table :fertilizers do |t|
      t.string :name
      t.string :category, :null => false
      t.float :n
      t.float :p
      t.float :k
      t.string :others

      t.timestamps
    end    
  end
end
