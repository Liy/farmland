class CreateGrows < ActiveRecord::Migration
  def change
    create_table :grows do |t|
      t.float :sown_area
      t.date :sowing_date
      t.integer :crop_id
      t.integer :farmer_id
      t.string :note

      t.timestamps
    end
  end
end
