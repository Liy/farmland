class CreateFertilizerApplications < ActiveRecord::Migration
  def change
    create_table :fertilizer_applications do |t|
      t.float :amount
      t.date :date
      t.integer :grow_id
      t.integer :fertilizer_id

      t.timestamps
    end
    add_index :fertilizer_applications, :grow_id
    add_index :fertilizer_applications, :fertilizer_id
    # allow same fertilizer apply to the same grow multiple times(probably in different time)
    # therefore, no join index.    
  end
end
