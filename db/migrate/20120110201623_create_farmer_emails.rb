class CreateFarmerEmails < ActiveRecord::Migration
  def change
    create_table :farmer_emails do |t|
      t.string :label
      t.string :value
      t.integer :farmer_id

      t.timestamps
    end
  end
end
