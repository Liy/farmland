class CreateUserTelecoms < ActiveRecord::Migration
  def change
    create_table :user_telecoms do |t|
      t.string :label
      t.string :value
      t.integer :user_id

      t.timestamps
    end
    add_index :user_telecoms, :user_id
  end
end
