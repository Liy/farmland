class CreateHarvestInfos < ActiveRecord::Migration
  def change
    create_table :harvest_infos do |t|
      t.date :date
      t.float :yield
      t.float :price
      t.integer :grow_id

      t.timestamps
    end
  end
end
