class CreateHarms < ActiveRecord::Migration
  def change
    create_table :harms do |t|
      t.string :name, :null => false
            
      t.timestamps
    end
  end
end
