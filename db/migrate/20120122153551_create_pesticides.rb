class CreatePesticides < ActiveRecord::Migration
  def change
    create_table :pesticides do |t|
      t.string :category
      t.string :name
      t.string :image
      
      t.timestamps
    end
    add_index :pesticides, :category
  end
end
