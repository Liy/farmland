class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username, :null => false, :uniqe => true
      t.string :email, :null => false, :unique => true
      t.string :password_digest, :null => false
      t.string :name, :null => false
      t.string :mobile
      t.string :avatar
      t.string :nickname
      
      # if presence, not activated
      t.string :activation_token
      
      # random token for password reset
      t.string :password_reset_token

      t.timestamps
    end
    
    add_index :users, :username
    add_index :users, :email
    add_index :users, :name
    add_index :users, :nickname
    add_index :users, :activation_token
    add_index :users, :password_reset_token
    
  end
end
