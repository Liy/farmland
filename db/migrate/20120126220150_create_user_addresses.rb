class CreateUserAddresses < ActiveRecord::Migration
  def change
    create_table :user_addresses do |t|
      t.string :label
      t.string :value
      t.integer :user_id

      t.timestamps
    end
    add_index :user_addresses, :user_id
  end
end
