class CreateUserEmails < ActiveRecord::Migration
  def change
    create_table :user_emails do |t|
      t.string :label
      t.string :value
      t.integer :user_id

      t.timestamps
    end
    add_index :user_emails, :user_id
  end
end
