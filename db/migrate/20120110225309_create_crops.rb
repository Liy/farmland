class CreateCrops < ActiveRecord::Migration
  def change
    create_table :crops do |t|
      t.string :category, :null => false
      t.string :variety
      t.string :quality
      t.date :growth_start, :null => false
      t.date :growth_end, :null => false
      t.integer :pesticide_residue_benchmark_id
      t.string :image

      t.timestamps
    end
  end
end
