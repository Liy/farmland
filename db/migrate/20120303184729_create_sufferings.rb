class CreateSufferings < ActiveRecord::Migration
  def change
    create_table :sufferings do |t|
      t.integer :grow_id
      t.integer :harm_id

      t.timestamps
    end
    add_index :sufferings, :grow_id
    add_index :sufferings, :harm_id
    
    # allow grow suffer same harm multiple times during the period.
  end
end
