class ImageTag < ActiveRecord::Base
  has_many :image_taggings, :dependent => :destroy
  has_many :images, :through => :image_taggings
  
  validates :name, :presence => true
  
end
