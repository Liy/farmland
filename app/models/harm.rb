class Harm < ActiveRecord::Base
  has_many :sufferings, :dependent => :destroy
  has_many :grows, :through => :sufferings
  
  validates :name, :presence => true
end
