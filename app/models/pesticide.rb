class Pesticide < ActiveRecord::Base
  has_many :pesticide_applications, :dependent => :destroy
  has_many :grows, :through => :pesticide_applications
  
  validates :name, :presence => true
  
  # allow elastic search to search record, add index
  include Tire::Model::Search
  # update the index when model is updated
  # include Tire::Model::Callbacks
  # TODO: remove this on production mode
  after_save do
    # db:populate rake task will set the seed to a certain value, in order 
    # to save time during seeding process. 
    self.update_index if ENV["seed"].nil?
  end
  after_destroy do
    self.update_index
  end
  
  def self.search(params)
    # page specify the start page, per_page specify how many items per page
    tire.search(load: true, page: params[:page], per_page: 10) do 
      query { 
        string params[:query], default_operator: 'AND'
      } if params[:query].present?
    end
  end
end
