class Suffering < ActiveRecord::Base
  attr_protected :grow_id
  
  belongs_to :grow
  belongs_to :harm
  
  # If the harm is destroyed, then the corresponding suffering model will be destroyed as well.
  # This also means you need to update the grow information as well
  after_destroy do
    self.grow.update_index
  end
end
