class User < ActiveRecord::Base
  attr_protected :user_id
  
  # tell rails to use its default password encryption
  has_secure_password
  
  validates :username, :presence => true,
                       :uniqueness => { :case_sensitive => false } 
  
  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i                   
  validates :email, :presence => true,
                    :format => { :with => email_regex },
                    :uniqueness => { :case_sensitive => false }
                    
  validates :name, :presence => true
                                       
  # I guess, everyone should have a mobile before you register as a pro                
  validates :mobile, :presence => true
  
  validates :password, :presence => true,
                       :length => { :within => 4..40 },
                       :confirmation => false
  
  # user have many guidances, if user is destroyed(the technician is dismissed), the guidances will be destroyed
  has_many :guidances, :dependent => :destroy
  # since guidances table is just a joint for user and farmer, therefore, we still need
  # to specify user can support multiple farmers at the sametime
  has_many :farmers, :through => :guidances
  
  has_many :user_emails
  has_many :user_addresses
  has_many :user_telecoms
  
  # privilliage
  has_many :assignments, :dependent => :destroy
  has_many :roles, :through => :assignments
  
  # can upload multiple images
  has_many :images
  
  # allows to upload a picture as the user's profile avatar through form.
  # In the database, there is a column is called avatar
  mount_uploader :avatar, AvatarUploader
  
  def support!(farmer, note=nil)
    guidances.create!(:farmer_id => farmer.id, :note => note)
  end
  
  # this generate a unique random token
  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end
  
  def send_registration_confirmation
    generate_token(:activation_token)
    # Although we only update the activation token, the validation will be processed by default
    # in order to save it correctly, we have to ignore validation
    save! :validate => false    
    UserMailer.registration_confirmation(self).deliver
  end
  
  # generate token and send out the email
  def send_password_reset
    generate_token(:password_reset_token)
    # Although we only update the reset token, the validation will be processed by default
    # in order to save it correctly, we have to ignore validation
    save! :validate => false
    UserMailer.password_reset(self).deliver
  end
  
  def activated?
    return self.activation_token.nil?
  end
  
  def activate
    # nil the token, indicate this account is activated
    self.activation_token = nil # why we have to use "self" ???
    
    save! :validate => false
  end
  
  def deactivate
    
  end
  
  # check if user is such role
  def has_role?(role_sym)
    # scan through all elements in roles array
    # if the conditional statment in the block ever return true,
    # the whole process will be true
    roles.any? do |role|
      role.name.underscore.to_sym == role_sym
    end
  end
  
  def assign!(role_sym)
    role = Role.where(:name => role_sym)[0]
    assignments.create!(:role_id => role.id) if role
  end
  
  def role=(role)
    assignments.create!(:role_id => role.id) if role
  end
  
  # pass :all to resign user completely
  def resign(role_sym)
    if role_syms == :all
      assignments.delete_all
    else
      role = Role.where(:name => role_sym)[0]
      assignment = assignments.find(role.id)[0]
      assignment.destroy if assignment
    end
  end
  
  
  # ====================================== microblog related methods
  def redis_key(type)
    return %(user:#{self.id}:#{type})
  end
  
  def follow!(user)
    REDIS.multi do
      # this current user is now following the target user
      REDIS.sadd(self.redis_key(:following), user.id)
      # this current user is now, a follower of the target user
      REDIS.sadd(user.redis_key(:followers), self.id)
    end
  end
  
  # For testing only
  def friend!(user)
    self.follow!(user)
    user.follow!(self)
  end
  
  def unfollow!(user)
    REDIS.multi do
      # this current user is no longer following the target user
      REDIS.srem(self.redis_key(:following), user.id)
      # this current user is no longer a follower of the target user
      REDIS.srem(user.redis_key(:followers), self.id)
    end
  end
  
  def followers
    follower_ids = REDIS.smembers(self.redis_key(:followers))
    return User.where(:id => follower_ids)
  end
  
  def following
    following_ids = REDIS.smembers(self.redis_key(:following))
    return User.where(:id => following_ids)
  end
  
  def friends
    # intersection of followers and following sets, will be the friends set.
    # friend has mutual relationship(you follows him, he also follows you)
    # note that, the params are two key names
    friend_ids = REDIS.sinter(
      self.redis_key(:followers), 
      self.redis_key(:following)
    )
    return User.where(:id => friend_ids)
  end
  
  def followed_by?(user)
    REDIS.sismember(redis_key(:followers), user.id)
  end
  
  def following?(user)
    REDIS.sismember(redis_key(:following), user.id)
  end
  
  def followers_count
    REDIS.scard(redis_key(:followers))
  end
  
  def following_count
    REDIS.scard(redis_key(:following))
  end
  
  # TODO: implement attachment
  def tweet(content)
    # convert the current time into integer as a score for personal timeline
    time_now = Time.now
    time_score = time_now.to_i
    
    tweet_id = REDIS.incr('tweet:next_id')
    core_id = REDIS.incr('core:next_id')
    
    REDIS.hmset( %(tweet:#{tweet_id}), 
      :id, tweet_id,
      :core_id, core_id,
      :pub_date, time_now )
      
    REDIS.hmset( %(core:#{core_id}),
      :id, core_id,
      :author, self.id,
      :content, content,
      :create_date, time_now )
    
    
    # push tweet to the global timeline
    # for displaying in the website's front page or something else... or monitoring
    # put the tweet into global tweets timeline
    REDIS.lpush( %(global:timeline), tweet_id )
    
    # for displaying the user's tweet on his home page
    # put the tweet into the current user's tweet timeline, this timeline contains only his own tweets and retweet
    REDIS.zadd( self.redis_key(:timeline), time_score, tweet_id )
    
    # put the tweet into the user's own inbox list
    REDIS.lpush( self.redis_key(:inbox), tweet_id )
    
    # put the tweet into the followers inbox list
    self.followers.each do |follower|
      REDIS.lpush( follower.redis_key(:inbox), tweet_id )
    end
    
    # TODO: implement mentions, notify them been mentioned
    
    return tweet_id
  end
  
  def reply(target_id, content, is_public)
    time_now = Time.now
    time_score = time_now.to_i
        
    reply_id = REDIS.incr('tweet:next_id')
    reply_core_id = REDIS.incr('core:next_id')
    
    REDIS.hmset( %(tweet:#{reply_id}), 
      :id, reply_id,
      :core_id, reply_core_id,
      :pub_date, time_now )
      
    REDIS.hmset( %(core:#{reply_core_id}),
      :id, reply_core_id,
      :author, self.id,
      :content, content,
      :create_date, time_now )
    
    # put reply to the user's timeline sorted set
    REDIS.zadd( %(user:#{self.id}:timeline), time_score, reply_id )
    # push reply to the user's inbox list
    REDIS.lpush( self.redis_key(:inbox), reply_id )
    
    
    # OVERVIEW of what should happen next:
    # the reply will be added to the reply target's replies list.
    # And if the reply target is not the root of conversation.(the tweet started
    # the conversation) Then the reply tweet_id will need to be added to the 
    # root tweet's replies list as well
    # 
    #
    #   replies       target replies
    #   ----------------------------------------------------------------------- left push 
    #   tweet:2       REPLY:7
    #   tweet:3       tweet:5     
    #   TARGET:4       
    #   tweet:5                                               ROOT:1
    #   tweet:6                           ROOT:1              tweet:2
    #   REPLY:7                           tweet:2             TARGET:4            
    #   ----------------------------------------------------------------------- right push
    #                                     target parents      reply parents
    #
    #
    
    # Push the reply tweet to the core of reply target's replies list
    target_core_id = REDIS.hget( %(tweet:#{target_id}), :core_id )
    REDIS.lpush( %(core:#{target_core_id}:replies), reply_id )
    
    
    # check if the target tweet's parents list exist, if it does not,
    # it means the target tweet is the root tweet no need to add the new tweet
    # to its replies list. if it DOES exist, then the target tweet is a children
    # of the FIRST element(root) of the target tweet's parents list, and you need to 
    # push the new tweet into the root's replies list
    target_parents_key = %(core:#{target_core_id}:parents)
    
    # Replying to the root tweet?
    
    # target tweet's parents list exists(not 0), you are REPLYING A REPLY of the root tweet
    # you need to add the reply to the root's replies as well
    is_replying_root = REDIS.llen(target_parents_key) == 0
    # replying a reply
    if( !is_replying_root ) 
      # get the all the target parent tweets's id
      target_parent_ids = REDIS.lrange( target_parents_key, 0, -1 )
      # root tweet id is the first one
      root_id = target_parent_ids[0]
      
      # add the reply id to the root tweet's replies list
      root_core_id = REDIS.hget( %(tweet:#{root_id}), :core_id )
      REDIS.rpush( %(core:#{root_core_id}:replies), reply_id )
      
      # update the reply target's parents list, the new list will include the reply target's tweet id
      REDIS.rpush( %(core:#{reply_core_id}:parents), target_parent_ids.push(target_id))
    else # replying root tweet
      # Any reply tweets must have their parents's list started with: a root tweet(which is not a reply tweet)
      # Update the reply's parents list by pushing the reply target tweet's id
      REDIS.rpush( %(core:#{reply_core_id}:parents), target_id )
    end
    
    
    # if the reply is public, then all his followers will be receiving the reply
    if(is_public)
      # put the tweet into the followers inbox sorted set
      self.followers.each do |follower|
        REDIS.lpush( follower.redis_key(:inbox), reply_id )
      end
      
      # This is a public reply, it will be added to the global timeline.
      REDIS.lpush( %(global:timeline), reply_id )
    else
      # if the reply target's author is the friend of him, then the reply will be pushed to his inbox.
      # Note that, the target tweet can be a root tweet.
      target_author_id = REDIS.hget( %(core:#{target_core_id}), :author )
      if(REDIS.sismember( %(user:#{self.id}:followers), target_author_id ))
        REDIS.lpush( %(user:#{target_author_id}:inbox), reply_id )
      end
      
      # If user is replying a reply! Then we need to decide whether to push the reply to the root tweet's
      # author's inbox
      if( !is_replying_root )
        root_author_id = REDIS.hget( %(core:#{root_core_id}), :author )
        # Only when the target(root) tweet's author also follows the replier, the reply tweet will be sent to
        # the target tweet's author's inbox
        if(REDIS.sismember( %(user:#{self.id}:followers), root_author_id ))
          REDIS.lpush( %(user:#{root_author_id}:inbox), reply_id )
        end
      end
    end
    
    # TODO: notify the author of reply target, "You got a new reply!"
    # TODO: implement mentions, notify them been mentioned

    return reply_id
  end
  
  def retweet(tweet_id)
    # note that, the follower's id is a type of integer, so you have to convert
    # the id to an integer first
    core_id = REDIS.hget("tweet:#{tweet_id}", :core_id)
    author_id = REDIS.hget("core:#{core_id}", :author).to_i
    
    # Get the retweet time
    time_now = Time.now
    time_score = time_now.to_i

    retweet_id = REDIS.incr('tweet:next_id')
    
    REDIS.hmset( %(tweet:#{retweet_id}), 
      :id, retweet_id,
      :core_id, core_id,
      :pub_date, time_now )
    
    # put the retweet to his own timeline
    REDIS.zadd( self.redis_key(:timeline), time_score, retweet_id )
    
    # Just push the retweet to the user's friend
    self.followers.each do |follower|
      # push it to follower's inbox
      REDIS.lpush( follower.redis_key(:inbox), retweet_id )
      
      # finally, the retweeted by key needs be set in order to tell followers who did the retweet
      REDIS.set( %(tweet:#{retweet_id}:retweeted_by), self.id )
    end
    
    # TODO: notify the tweet's author, his tweet is retweed by the user: self.id 
  end
  
  def timeline
    tweet_ids = REDIS.zrevrange(self.redis_key(:timeline), 0, -1)
    
    tweet_ids.map do |id|
      tweet_hash = REDIS.hgetall(%(tweet:#{id}))
      core_hash = REDIS.hgetall(%(core:#{tweet_hash["core_id"]}))
      
      retweeted_by_id = REDIS.get( %(tweet:#{id}:retweeted_by) )
      retweeted_by_name = User.find_by_id( retweeted_by_id ).name if retweeted_by_id
      
      tweet = { 
        :id => id, 
        :core_id => tweet_hash["core_id"],
        :pub_date => tweet_hash["pub_date"],
        :author => User.find_by_id(core_hash["author"]).name,
        :author_id => core_hash["author"],
        :retweeted_by => retweeted_by_name,
        :retweeted_by_id => retweeted_by_id,
        :content => core_hash["content"],
        :create_date => core_hash["create_date"] 
      }
    end
  end
  
  def inbox
    tweet_ids = REDIS.lrange(self.redis_key(:inbox), 0, -1)
    
    tweet_ids.map do |id|
      tweet_hash = REDIS.hgetall(%(tweet:#{id}))
      core_hash = REDIS.hgetall(%(core:#{tweet_hash["core_id"]}))
      
      retweeted_by_id = REDIS.get( %(tweet:#{id}:retweeted_by) )
      retweeted_by_name = User.find_by_id( retweeted_by_id ).name if retweeted_by_id
      
      tweet = { 
        :id => id, 
        :core_id => tweet_hash["core_id"],
        :pub_date => tweet_hash["pub_date"],
        :author => User.find_by_id(core_hash["author"]).name,
        :author_id => core_hash["author"],
        :retweeted_by => retweeted_by_name,
        :retweeted_by_id => retweeted_by_id,
        :content => core_hash["content"],
        :create_date => core_hash["create_date"]
      }
    end
  end
  
  def reply_parents(reply_id)
    core_id = REDIS.hget("tweet:#{reply_id}", :core_id)
    parent_ids = REDIS.lrange("core:#{core_id}:parents", 0, -1)
  end
end