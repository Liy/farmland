class Fertilizer < ActiveRecord::Base
  has_many :fertilizer_applications, :dependent => :destroy
  has_many :grows, :through => :fertilizer_applications
end
