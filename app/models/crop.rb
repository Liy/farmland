class Crop < ActiveRecord::Base
  ###########
  # TODO: refine this... add confirmation
  # Note this is quite dangerous to destroy all
  # Maybe in the future, do not allow user to destroy a 
  # crop if there are grows are referencing the specific crop?
  ###########
  has_many :grow, :dependent => :destroy
  
  # has_one :pesticide_residue_benchmark
  
  # enable undo and redo action for this model
  has_paper_trail
  
  # allow elastic search to search record, add index
  include Tire::Model::Search
  # update the index when model is updated
  # include Tire::Model::Callbacks
  # TODO: remove this on production mode
  after_save do
    # db:populate rake task will set the seed a certain value, in order 
    # to save time during seeding process. In the 
    self.update_index if ENV["seed"].nil?
  end
  after_destroy do
    self.update_index
  end
  
  def self.search(params)
    tire.search do
      query { string params[:query], default_operator: 'AND', size: 100 } if params[:query].present?
    end
  end
  
  # mapping do
  #   indexes :id, type: 'integer'
  #   indexes :catetory
  #   indexes :variety
  #   indexes :growth_start, type: 'date'
  #   indexes :growth_end, type: 'date'
  #   indexes :quality
  # end
  # 
  # def self.search(params)
  #   tire.search do  
  #     query do
  #       boolean do
  #         must { string params[:query], default_operator: "AND" } if params[:query].present?
  #       end
  #     end
  #   
  #     sort { by :id, 'asc' } if params[:query].blank?
  #   end
  # end
  
end
