class FarmerEmail < ActiveRecord::Base
  belongs_to :farmer
  
  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :value, :format => { :with => email_regex }
end
