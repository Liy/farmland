#coding: utf-8
require 'file_size_validator' 

class Image < ActiveRecord::Base
  belongs_to :user
  
  # has many image tags, and once a image is deleted, all its tags must be destroyed as well
  has_many :image_taggings, :dependent => :destroy
  has_many :image_tags, :through => :image_taggings
  
  # the hash of the image content
  attr_accessor :image_hash
  
  # The uploader will use 'image' to hold the uploader instance, but the path will be stored 
  # into database's 'filename' column
  mount_uploader :image, ImageUploader, :mount_on => :filename
  
  # validates :image, :file_size => { :maximum => 10.megabytes.to_i }
  
  # virtual attirbutes, since there is a custom tag_name getter defined, only setter
  # is needed. 
  attr_writer :tag_names
  
  # assign tags after image is succesfully saved.
  after_save :assign_tags
  
  # allow elastic search to search record, add index
  include Tire::Model::Search
  # update the index when model is updated
  # include Tire::Model::Callbacks
  # TODO: remove this on production mode
  after_save do
    # db:populate rake task will set the seed a certain value, in order 
    # to save time during seeding process. In the 
    self.update_index if ENV["seed"].nil?
  end
  after_destroy do
    self.update_index
  end
  
  mapping do
    indexes :id, type: 'string', index: 'not_analyzed', include_in_all: false
    indexes :date_time_original, type: 'date'
  end
  
  def to_indexed_json
    {
      :tag => self.image_tags,
      :uploader => self.user.name,
      :serial_number => self.serial_number,
      :date_time_original => self.date_time_original,
      :tag_ids => self.image_tag_ids
    }.to_json
  end
  
  def self.search(params)
    tire.search(load: true) do
      query{ 
        string params[:query], default_operator: "AND" 
        
      } if params[:query].present?
      
      # define image tags facet
      facet 'tags' do
        # by default only 10 facets will be created, therefore, make sense to set it to the 
        # total number of the tags in the database
        terms(:tag_ids, :size => ImageTag.all.count)
      end
      
      # filter the search result depends on the selected facet(s)
      filter :term, tag_ids: params[:tag_ids] if params[:tag_ids].present?      
    end
  end
  
  # get the tags of this image
  def tag_names
    # If there is a validation error, the tags will not be saved, but the input tags
    # will still be store in the instance variable.(through setter genearated by 'attr_writer')
    # 
    # otherwise, if it is first time try to read the tags, present the entries in the image_tags table.
    @tag_names || self.image_tags.map(&:name).join(',')
  end
  
  # DateTimeOriginal
  # def get_exif(name)
  #   manipulate! do |img|
  #     return img["EXIF:" + name]
  #   end
  # end
  
  # This method is called before image is saved, which save some exif information
  # and generate sha1 file name base on the file content. Called in controller, right before
  # save method calling.
  def pre_process
    # img = MiniMagick::Image.open(image.path)
    # if !img.nil?
    #   dt = img["EXIF:DateTimeOriginal"].to_s
    #   self.taken_at = DateTime.strptime(dt, '%Y:%m:%d %H:%M:%S') unless dt.blank?
    #   self.serial = img["EXIF:SerialNumber"]
    #   self.lens_model = img["EXIF:Orientation"]
    #   self.info = img
    # end
    
    # img = Magick::Image.read(image.path)[0] rescue nil
    # dt = img["EXIF:DateTimeOriginal"].to_s
    # self.taken_at = DateTime.strptime(dt, '%Y:%m:%d %H:%M:%S') unless dt.blank?
    # # In the future, I may not want to directly rotate and save image, I may just apply a 
    # # rotation transformation using javascript or css to display the correct orientated image
    # self.orientation = img['EXIF:Orientation']
    # self.serial = img['EXIF:SerialNumber']
    # self.lens_model = img["EXIF:LensModel"]
    
    # I need to use the stream to generate the file name
    stream = File.open(image.path, 'rb')
    # create hash from the stream
    self.image_hash = Digest::SHA1.hexdigest(stream.read)
    
    img = MiniMagick::Image.read(stream) rescue nil
    dt = img["EXIF:DateTimeOriginal"].to_s
    self.date_time_original = DateTime.strptime(dt, '%Y:%m:%d %H:%M:%S') unless dt.blank?
    # In the future, I may not want to directly rotate and save image, I may just apply a 
    # rotation transformation using javascript or css to display the correct orientated image
    self.orientation = img['EXIF:Orientation']
    self.serial_number = img['EXIF:SerialNumber']

    # just testing text for record exif....
    # File.open("#{Rails.root}/test.txt", 'w') do |f2|
    #     f2.puts img.run_command("identify -format %[exif:*] #{img.path}") 
    # end
    
    
    # TODO: testing, to be deleted
    category_tags = ['水稻', '小麦', '玉米', '油菜', '苹果', '草莓', '菊花', '牡丹', '梅花', '水仙', '风信子', '葵花']
    harm_tags =['真菌', '细菌', '病毒', '作物', '动物', '老鼠', '蚜虫', '千牛']
    part_tags = ['苗', '根', '茎', '叶', '花', '果', '全株']
    @tag_names = [category_tags.sample(1), harm_tags.sample(rand(5)), part_tags.sample(1)].flatten.join(',')
  end
  
  def to_jq_upload
    {
      "tags" => @tag_names,
      "name" => read_attribute(:filename),
      "size" => image.size,
      "url" => image.url,
      "thumbnail_url" => image.thumb.url,
      "delete_url" => image.path,
      "delete_type" => "DELETE"
    }
  end
  
  
  private
  
  def assign_tags
    # if there is tag names input is provided
    if @tag_names
      # 'small comma' and 'full width comma' are both treated as a tag delimiter
      self.image_tags = @tag_names.split(/\uFE50|\uFF0C|\,+/).reject { |name|
        # strip all the newline character and spaces at the head or tail of the string
        name.strip!
        # if after strip the name is empty, reject it
        name.empty?
      }.map { |name|
        # return the tag if it is exist and if not, create the tag
        # the purpose here is ensure only no duplicated tag is created in the
        # image_tags table.
        ImageTag.find_or_create_by_name(name)
      }
    end
  end
  
end
