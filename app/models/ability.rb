class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
    
    @user = user || User.new # for guest
    @user.roles.each { |role| send(role.name.downcase) }
    
    # guest cannot do anything
    # other than view home page and sign in. Sign in will be handled mannually
  end
  
  # staff can only collect the farmer's data, thus it is a good idea not allow then to destroy farmer 
  # or grow information
  # Crop data, I don't know whether they will need to be able to manage it or not. Read only for now.
  # User can view everyone's information, but can only update his own information.
  def staff
    can [:read, :create, :update], [Farmer, Grow]
    can :all, Grow
    can :read, Crop
    
    # Can read all user's information
    can :read, User
    # But can only manage his own user account.
    can :manage, User, :id => @user.id
    
    # Can only show, new, and update his own guidances
    can [:read, :create, :update], Guidance, :user => { :id => @user.id }
    
    # staff can read, upload image
    can [:read, :create, :upload], Image
    # but can only update and destroy their own creation
    can [:update, :destroy], Image, :user_id => @user.id
    
    # can manage the tweets of their own
    can :manage, Tweet
  end
  
  # supervisor will not be able to destroy user(fire a user?)
  def supervisor
    can :manage, [Farmer, Crop, Grow]
    # user's creation(user registration) is handled manually(through ip address check), not by CanCan
    can [:read, :update], User
    
    # can manage all the guidances
    can :manage, Guidance
    
    # can mange image
    can :manage, Image
    
    # can manage the tweets of their own
    can :manage, Tweet
  end
  
  # manager with super power can...blow up the universe...
  def manager
    can :manage, :all
  end
end
