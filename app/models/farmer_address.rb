class FarmerAddress < ActiveRecord::Base
  belongs_to :farmer
  
  def value
    addr = ""
    self.attributes.values[2..4].each do |value|
      addr += value.to_s
      addr += " " unless value.to_s.empty?
    end
    addr += attributes.values[5].to_s

    return addr
  end
end
