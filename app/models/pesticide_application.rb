class PesticideApplication < ActiveRecord::Base
  attr_protected :grow_id
  
  belongs_to :grow
  belongs_to :pesticide
  
  # TDDO: It's all because of the facet search in grow... the pesticide index is
  # only updated when grow itself is updated. How to optimize this!??!
  #
  # In order to reflect the deletion of pesticide to corresponding grow,
  # we have to call grow's update_index method when pesticide is deleted.
  #
  # Note that:
  #     has_many :pesticide_applications, :dependent => :destroy
  # ensures the corresponding pesticide_application entry is detroyed automatically when
  # a pesticde is deleted.
  #
  # Also note that:
  # we only interested in pesticide_application's deletion, not update,
  #
  # The same thing is also happend with harm models(suffering)
  after_destroy do
    self.grow.update_index
  end
end
