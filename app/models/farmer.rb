class Farmer < ActiveRecord::Base
  # once farmer is destroyed, the association between him and user is auto destroyed
  has_many :guidances, :dependent => :destroy
  has_many :users, :through => :guidances
  
  has_many :addresses, :class_name => 'FarmerAddress', :dependent => :destroy
  has_many :telecoms, :class_name => 'FarmerTelecom', :dependent => :destroy
  has_many :emails, :class_name => 'FarmerEmail', :dependent => :destroy
  
  has_many :grows, :dependent => :destroy
  
  validates :name, :presence => true
  
  # accepts multiple telecom forms
  accepts_nested_attributes_for :telecoms, :allow_destroy => true, :reject_if => proc { |attr_hash| attr_hash[:value].blank? }
  
  
  # accepts mutiple email forms
  accepts_nested_attributes_for :emails, :allow_destroy => true, :reject_if => lambda { |attrs_hash| attrs_hash[:value].blank? }
  
  
  # reject certain empty address submitted
  accepts_nested_attributes_for :addresses, :allow_destroy => true, :reject_if => :addr_reject_check
  
  # allow elastic search to search record, add index
  include Tire::Model::Search
  # update the index when model is updated
  # include Tire::Model::Callbacks
  # TODO: remove this on production mode
  after_save do
    # db:populate rake task will set the seed to a certain value, in order 
    # to save time during seeding process. 
    self.update_index if ENV["seed"].nil?
  end
  after_destroy do
    self.update_index
  end
  
  # process to check whether to reject a certain submit of the farmer's addresses,
  # if all the address fields are empty, then reject the submit
  def addr_reject_check (attrs_hash)
    # FIXME: for some reason, 1..-1 will not work...
    attrs_hash.values[1..5].each do |v|
      if !v.blank?
        return false
      end
    end
    return true
  end
  
  mapping do
    indexes :id, type: 'string', index: 'not_analyzed', include_in_all: false
    indexes :arable, type: 'float'
    indexes :cultivated, type: 'float'
  end
  
  def to_indexed_json
    {
      name: self.name,
      arable: self.arable,
      cultivated: self.cultivated,
      emails: self.emails.map{ |email|
        {
          label: email.label,
          email: email.value
        }
      },
      addresses: self.addresses.map{ |addr|
        {
          label: addr.label,
          address: addr.value,
          postcode: addr.postcode,
          city: addr.city,
          province: addr.province
        }
      },
      telecoms: self.telecoms.map{ |telecom|
        {
          label: telecom.label,
          telecom: telecom.value
        }
      },
      user_ids: self.user_ids
    }.to_json
  end
  
  def self.search(params)
    # page specify the start page, per_page specify how many items per page
    tire.search(load: true, page: params[:page], per_page: 10) do 
      query { 
        string params[:query], default_operator: 'AND'
      } if params[:query].present?
      
      # define image tags facet
      facet 'tags' do
        # by default only 10 facets will be created, therefore, make sense to set it to the 
        # total number of the entries in the database
        terms(:user_ids, :size => User.all.count)
      end
      
      # filter the search result depends on the selected facet(s)
      filter :term, user_ids: params[:user_ids] if params[:user_ids].present?
    end
  end
end
