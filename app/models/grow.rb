class Grow < ActiveRecord::Base
  attr_protected :farmer_id
  
  belongs_to :farmer
  
  belongs_to :crop
  has_one :harvest_info
  
  has_many :fertilizer_applications, :dependent => :destroy
  has_many :fertilizers, :through => :fertilizer_applications
  
  has_many :pesticide_applications, :dependent => :destroy
  has_many :pesticides, :through => :pesticide_applications
  
  has_many :sufferings, :dependent => :destroy
  has_many :harms, :through => :sufferings
  
  # allow elastic search to search record, add index
  include Tire::Model::Search
  # update the index when model is updated
  # include Tire::Model::Callbacks
  # TODO: remove this on production mode
  after_save do
    # db:populate rake task will set the seed a certain value, in order 
    # to save time during seeding process. In the 
    self.update_index if ENV["seed"].nil?
  end
  after_destroy do
    self.update_index
  end
  
  mapping do
    indexes :id, type: 'string', index: 'not_analyzed', include_in_all: false
    indexes :sown_area, type: 'float'
    indexes :sowing_date, type: 'date'
  end
  
  def to_indexed_json
    {
      farmer: self.farmer.name,
      crop: {
        id: self.crop_id,
        category: self.crop.category
      },
      sown_area: self.sown_area,
      sowing_date: self.sowing_date,
      crop_id: self.crop_id,
      harm_ids: self.harm_ids,
      pesticide_ids: self.pesticide_ids,
    }.to_json
  end
  
  def self.search(params)
    # page specify the start page, per_page specify how many items per page
    tire.search(load: true, page: params[:page], per_page: 20) do 
      query { 
        string params[:query], default_operator: 'AND'
      } if params[:query].present?
      
      # define image tags facet
      facet 'crop_tags' do
        # by default only 10 facets will be created, therefore, make sense to set it to the 
        # total number of the entries in the database
        terms(:crop_id, :size => Crop.all.count)
      end
      
      # filter the search result depends on the selected facet(s)
      filter :term, crop_id: params[:crop_id] if params[:crop_id].present?
      
      
      # pesticide tags facets
      facet 'pesticide_tags' do
        # by default only 10 facets will be created, therefore, make sense to set it to the 
        # total number of the entries in the database
        terms(:pesticide_ids, :size => Pesticide.all.count)
      end
      
      # filter the search result depends on the selected facet(s)
      filter :term, pesticide_ids: params[:pesticide_ids] if params[:pesticide_ids].present?
      
      # pesticide tags facets
      facet 'harm_tags' do
        # by default only 10 facets will be created, therefore, make sense to set it to the 
        # total number of the entries in the database
        terms(:harm_ids, :size => Harm.all.count)
      end
      
      # filter the search result depends on the selected facet(s)
      filter :term, harm_ids: params[:harm_ids] if params[:harm_ids].present?      
      
    end
  end
  
  def suffer!(harm)
    sufferings.create!(:harm_id => harm.id)
  end
  
  def fertilize!(fertilizer, amount, date)
    fertilizer_applications.create!(:fertilizer_id => fertilizer.id, :amount => amount, :date => date)
  end
  
  def use_pesticide!(pesticide, amount, date)
    pesticide_applications.create!(:pesticide_id => pesticide.id, :amount => amount, :date => date)
  end
end
