class ImageTagging < ActiveRecord::Base
  # I guess if there is a image edit restrict, image_id mass assignment must NOT be allowed.
  # Since user might not allow to assign tags to a certain images(images that not owned by them)
  attr_protected :image_id
  
  belongs_to :image
  belongs_to :image_tag
end
