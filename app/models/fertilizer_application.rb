class FertilizerApplication < ActiveRecord::Base
  # user might not allow to update certain grow information, therefore, mass assignment must be
  # restricted
  attr_protected :grow_id
  
  belongs_to :fertilizer
  belongs_to :grow
end
