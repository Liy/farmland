# guidances table is a joint table between users and famers
class Guidance < ActiveRecord::Base
  attr_protected :user_id
  
  belongs_to :user
  belongs_to :farmer
end
