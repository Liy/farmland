class Tweet < RedisBase
  
  # Find the tweet by tweet id
  def self.find_by_id(*ids)
    ids.flatten.compact.uniq.map do |id|
      tweet_hash = REDIS.hgetall(%(tweet:#{id}))
      core_hash = REDIS.hgetall(%(core:#{tweet_hash["core_id"]}))
      
      retweeted_by_id = REDIS.get( %(tweet:#{id}:retweeted_by) )
      retweeted_by_name = User.find_by_id( retweeted_by_id ).name if retweeted_by_id
      
      hashes = {
        'id' => id,
        'core_id' => tweet_hash['core_id'],
        'pub_date' => tweet_hash['pub_date'],
        'author' => User.find_by_id(core_hash["author"]).name,
        'author_id' => core_hash["author"],
        'retweeted_by' => retweeted_by_name,
        'retweeted_by_id' => retweeted_by_id,
        'create_date' => core_hash['create_date'],
        'content' => core_hash['content'],
        'create_date' => core_hash["create_date"]
      }
    end
  end
  
  def self.retweeted_by(retweeted_id)
    return REDIS.get("tweet:#{retweeted_id}:retweeted_by")
  end
  
  def self.replies(core_id)
    tweet_ids = REDIS.lrange("core:#{core_id}:replies", 0, -1)
    return Tweet.find_by_id(tweet_ids)
  end
  
  def self.parents(core_id)
    tweet_ids = REDIS.lrange("core:#{core_id}:parents", 0, -1)
    return Tweet.find_by_id(tweet_ids)
  end
  
  def self.retweets(core_id)
    return REDIS.smembers("core:#{core_id}:retweets")
  end
  
  def self.favourites(core_id)
    return REDIS.smembers("core:#{core_id}:favourites")
  end
  
end