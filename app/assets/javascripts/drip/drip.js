function Drip(target){
	target.addEventListener("dragenter",  function(evt){
        evt.stopPropagation();
        evt.preventDefault();
	}, false);
    target.addEventListener("dragleave",  function(evt){
		evt.stopPropagation();
        evt.preventDefault();
		this.setAttribute('class', 'drip drip-leave');
	}, false);
    target.addEventListener("dragover",  function(evt){
		evt.stopPropagation();
        evt.preventDefault();
		this.setAttribute('class', 'drip drip-dragover');
	}, false);
    target.addEventListener("drop",  function(evt){
		evt.stopPropagation();
        evt.preventDefault();
		this.setAttribute('class', 'drip')
	}, false);
};