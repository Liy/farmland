clientSideValidations.callbacks.element.fail = function(element, message, callback) {
	callback();
	if (element.data('valid') !== false) {
		element.parent().find('.message').hide().fadeIn('fast');
		element.closest('.control-group').removeClass('success').addClass('error');
	}
}

clientSideValidations.callbacks.element.pass = function(element, callback) {
  	// Take note how we're passing the callback to the hide() 
  	// method so it is run after the animation is complete.
    element.parent().find('.message').fadeOut('slow');
    element.closest('.control-group').removeClass('error').addClass('success');
}

// clientSideValidations.callbacks.element.after = function(element, callback) {
// 	console.log(element);
// 	// valid
// 	if(element.data('valid') == null){
// 		element.parent().find('.message').fadeOut('slow');
// 	    element.closest('.control-group').removeClass('error').addClass('success');
// 	}
// }