function CandyBox(container, sideLen){
	this.candies = [];
	this.container = container;
	this.container.style.position = 'relative';
	
	if(sideLen == null){
		this.sideLen = 256;
	}
	else{
		this.sideLen = sideLen;
	}
	
	this.width = 0;
	this.height = 0;
	
	this.rows = 0;
	this.columns = 0;
	
	this.contentWidth = 0;
	this.contentHeight = 0;
	
	this.lineSpace = 30;
	this.itemSpace = 20
};

CandyBox.prototype.addFile = function(fileInfo){
	var candy = new Candy(this.sideLen);
	candy.setLabel(fileInfo.name);

	
	// image will be wrapped by a div element
	candy.wrapper.style.position = "absolute";
	this.candies.push(candy);
	
	this.container.appendChild(candy.wrapper);
	
	this.update();
	
	return candy;
};

CandyBox.prototype.addImage = function(image, fileInfo){
	var candy = new Candy(this.sideLen);
	
	// draw the image to the canvas element
	candy.drawImage(image);
	// set the label
	candy.setLabel(fileInfo.name);
	
	// image will be wrapped by a div element
	candy.wrapper.style.position = "absolute";
	this.candies.push(candy);
	
	this.container.appendChild(candy.wrapper);
	
	this.update();
	
	return candy;
}

CandyBox.prototype.removeCandy = function(candy){
	container.removeChild(candy.wrapper);

	delete candies[candies.indexof(candy)];
		
	this.update();
}

CandyBox.prototype.update = function(){
	var tx = 0;
	var ty = 0;
	
	// reset content height in order to track the actual content height
	this.contentHeight = 0;
	
	var len = this.candies.length;
	
	// row
	this.rows = 0;
	if(len != 0){
		this.rows = 1;
	}
	
	for(var i=0; i<len; ++i){
		// tack content height
		var tempHeight = ty + this.candies[i].height
		if(tempHeight > this.contentHeight){
			this.contentHeight = tempHeight;
		}
		
		if(tx + this.candies[i].width > this.width){
			tx = 0;
			ty += this.candies[i].height + this.lineSpace;
			
			// update the container height
			if(ty > this.height){
				this.height = ty;
				this.container.style.height = ty + this.candies[i].height + "px";
			}
			
			// increament rows
			++this.rows;
		}
		
		this.candies[i].wrapper.style.left = tx+"px";
		this.candies[i].wrapper.style.top = ty+"px";
		
		tx += this.candies[i].width + this.itemSpace;
	}
	
	// calculate row and column
	this.columns = Math.ceil(len/this.rows);
	// calculate content width
	this.contentWidth = this.columns*this.sideLen + (this.columns-1)*this.itemSpace;
	
	console.log(this.rows, this.columns);
	console.log(this.contentWidth, this.contentHeight);
}

CandyBox.prototype.resize = function(w, h){
	this.width = w;
	this.height = h;
	
	this.container.style.width = w + "px";
	this.container.style.height = h + "px";
	
	this.update();
}

CandyBox.prototype.clear = function(){
	for(var candy in candies ){
		container.removeChild(candy.wrapper);
	}
	candies = [];
}