function Candy(sideLen){
	this.labelHeight = 0;
	this.labelSpace = 5;
	this.width = this.height = sideLen;
	
	this.wrapper = document.createElement('div');
	this.wrapper.setAttribute('class', 'wrapper')
	this.wrapper.style.width = this.width+"px";
	this.wrapper.style.height = this.height+"px";
	
	this.canvas = null;
	this.div = document.createElement('div');
	this.div.setAttribute('class', 'candy');
	this.div.style.width = this.width+"px";
	this.div.style.height = this.height-this.labelHeight-this.labelSpace+"px";
	this.wrapper.appendChild(this.div);
};

Candy.prototype.setLabel = function(str){
	this.labelHeight = 20;
	this.label = document.createElement('div');
	this.label.setAttribute('class', 'image-label label');
	this.label.innerHTML = str
	this.label.style.width = this.width*0.8 + "px";
	this.label.style.height = this.labelHeight + "px";
	this.wrapper.appendChild(this.label);
	this.label.style.position = "relative";
}

Candy.prototype.setCanvas = function(canvas){
	this.canvas = canvas;
	if(this.div.childNodes[0]){
		this.div.removeChild(this.div.childNodes[0]);
	}
	this.div.appendChild(canvas);
	
	// center the canvas
	this.canvas.style.position = "relative";
	this.canvas.style.left = (this.width - this.canvas.width)/2 + "px";
	this.canvas.style.top = (this.height - this.labelHeight - this.canvas.height)/2 + "px";
}

Candy.prototype.cloneCanvas = function(){
	if(this.canvas){
		var canvas = document.createElement('canvas');
		var ctx = canvas.getContext('2d');
		ctx.drawImage(this.canvas, 0, 0, this.canvas.width, this.canvas.height);
		return canvas;
	}
	return null;
}

Candy.prototype.clone = function(){
	var candy = new Candy(this.sideLen);
	candy.setCanvas(this.cloneCanvas);
	return candy;
}

Candy.prototype.drawImage = function(img){
	var canvas = document.createElement('canvas');
	
	var ctx = canvas.getContext('2d');
	
	var sx = this.width/img.width;
	var sy = (this.height-this.labelHeight-this.labelSpace)/img.height;
	
	if(sx < sy)
		sy = sx;
	else
		sx = sy;
		
	canvas.width = img.width * sx;
	canvas.height = img.height * sy;
		
	ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
	
	this.setCanvas(canvas);
}

Candy.prototype.prompt = function(type){
	this.div.setAttribute('class', 'candy-'+type);
	if(type == 'success')
		this.label.setAttribute('class', 'label image-label label-success');
	else if(type == 'fail')
		this.label.setAttribute('class', 'label image-label label-fail');
	else if(type == 'loading')
		this.label.setAttribute('class', 'label image-label label-warning');
}
	