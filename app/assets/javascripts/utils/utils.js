/**
 * Get available dimension of the browser
 **/
function getInnerDims(){
	if(typeof(window.innertWidth) == 'number'){
		return [window.innerWidth, window.innerHeight];
	}
	else if(document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight)){
		return [document.documentElement.clientWidth, document.documentElement.clientHeight];
	}
	else{
		return [document.body.clientWidth, document.body.cilentHeight];
	}
}