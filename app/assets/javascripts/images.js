$(function () {
	var dataList = [];
	
	
	// initilize image grid to accept images
	var imageContainer = document.getElementById('image-container');
	
	var drip = new Drip(imageContainer);
	
	var box = new CandyBox(imageContainer, 128);
	var initialHeight = getInnerDims()[1] - 50 - parseInt(document.getElementById('footer').offsetHeight);
	// there are 20 pixels padding on left and right of the container
	box.resize(parseInt(imageContainer.parentNode.offsetWidth) - 40, initialHeight);
	
	
	$('#fileupload').bind('fileuploadprogressall', function(e, data){
		$('#image-list').text(parseInt(data.loaded / data.total * 100, 10));
	});

    $('#fileupload').fileupload({
        dataType: 'json',
		dropZone: $('#image-container'),
        url: 'upload',
        done: function (e, data) {
            $.each(data.result, function (index, file) {
				// alert("index: " + data.candy);
				data.candy.prompt('success');
            });
        },
		change: function(e, data){
			// file input chnages, re-layout everything
			// alert(data.files.length);
		},
    	add: function(e, data){
			// append thumbnail
			if (window.FileReader){
				var reader = new FileReader();
				reader.file = data.files[0];
				reader.onloadend = function(e){
					var img = new Image();
					img.onload = function(){
						// add image to the grid, record the corresponding dom element
						data.candy = box.addImage(img, {name:reader.file.name});

						dataList.push(data);
					}
					img.onerror = function(){
						alert('load error');
					}
					// load the image
					img.src = e.target.result;
				};
				// read the file
				reader.readAsDataURL(data.files[0]);
			}
			else{
				var file = data.files[0];
				data.candy =  box.addFile({name:file.name});
				
				dataList.push(data);
			}
		}
	});
	
	$('#upload-btn').bind('click', function(){
		// alert('dataList.length: ' + dataList.length);
		
		for(var i=0; i<dataList.length; ++i){
			// set all images to loading state
			dataList[i].candy.prompt('loading');
			// alert('data: ' + dataList[i]);
			dataList[i].submit();
		}
		dataList = [];
	});
	
	
	
});