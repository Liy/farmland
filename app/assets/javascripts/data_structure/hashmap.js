/**
 * If you want to extend this class, you have to make sure all the extended methods and property
 * are non-enumerable. The only thing enumerable is the keys of the the map.
 *
 * Note that, this HashMap class simply assign key as a property. Therefore, 
 * if you want to use an Object as a key, the Object instance must implement a hashCode()
 * method which should return a unique String representation(time stamped? object builder index?) 
 * of the instance.
 * 
 * I really don't want to add a hashCode() into Object prototype, since it is such a base class,
 * and it difficult to create a correct hashCode() method using current existing functionality in javascript.
 *
 * TODO: decide how to represent the size of the map...using local size variable or iterate all keys
 * every time size() is called. The latter case ensures map['key'] = value assignment still return
 * correct size.
 *
 * @auther Liy
 **/ 
function HashMap(){
	// find a better way to declare the size
	Object.defineProperty(this, 'size', {
		value: 0, 
		writable: true, 
		enumerable: false,
		configurable: false
	});
}

// seems like only string typed key is reliable
Object.defineProperty(HashMap.prototype, 'put', {
	value: function(key, value){
		if(key == null || key == undefined)
			throw new Error('key cannot be null or undefined');
		
		// if the key has a hashCode function then use the hashCode as the key
		if(typeof key.hashCode == 'function'){
			key = key.hashCode();
		}
		
		if(!this.hasOwnProperty(key)){
			++this.size;
		}
		this[key] = value;
	},
	writable: false, 
	enumerable: false,
	configurable: false
});

Object.defineProperty(HashMap.prototype, 'get', {
	value: function(key){
		if(key == null || key == undefined)
			throw new Error('key cannot be null or undefined');
			
		// if the key has a hashCode function then use the hashCode as the key
		if(typeof key.hashCode == 'function'){
			key = key.hashCode();
		}
		
		return this[key];
	},
	writable: false, 
	enumerable: false,
	configurable: false
});

Object.defineProperty(HashMap.prototype, 'containsKey', {
	value: function(key){
		if(key == null || key == undefined)
			throw new Error('key cannot be null or undefined');
			
		// if the key has a hashCode function then use the hashCode as the key
		if(typeof key.hashCode == 'function'){
			key = key.hashCode();
		}
		
		return this.hasOwnProperty(key);
	},
	writable: false, 
	enumerable: false,
	configurable: false
});

// This method is slow, since it will need to enumerate the keys first
Object.defineProperty(HashMap.prototype, 'containsValue', {
	value: function(value){
		for(var key in this){
			// no need to check the key has hashCode or not.
			// since it is always a string, we can saftly use it to get the value.
			if(this[key] == value)
				return true;
		}
		return false;
	},
	writable: false, 
	enumerable: false,
	configurable: false
});

Object.defineProperty(HashMap.prototype, 'remove', {
	value: function(key){
		if(key == null || key == undefined)
			throw new Error('key cannot be null or undefined');
		// if the key has a hashCode function then use the hashCode as the key
		if(typeof key.hashCode == 'function'){
			key = key.hashCode();
		}
		
		if(this.hasOwnProperty(key)){
			var value = this[key];
			delete this[key];

			--this.size;

			return value;
		}
		else{
			return null;
		}
	},
	writable: false, 
	enumerable: false,
	configurable: false
});

// This method is slow, since it needs to enumerate the keys
Object.defineProperty(HashMap.prototype, 'keys', {
	value: function(){
		var keys = [];
		for(key in this){
			keys.push(key);
		}
		return keys;
	}, 
	writable: false, 
	enumerable: false,
	configurable: false
});

// This method is slow, since it needs to enumerate the keys first
Object.defineProperty(HashMap.prototype, 'values', {
	value: function(){
		var values = [];
		for(key in this){
			values.push(this[key]);
		}
		return values;
	}, 
	writable: false, 
	enumerable: false,
	configurable: false
});

Object.defineProperty(HashMap.prototype, 'clear', {
	value: function(){
		for(key in this){
			delete this[key];
		}
		this.size = 0;
	}, 
	writable: false, 
	enumerable: false,
	configurable: false
});

Object.defineProperty(HashMap.prototype, 'isEmpty', {
	value: function(){
		return this.size == 0;
	}, 
	writable: false, 
	enumerable: false,
	configurable: false
});

Object.defineProperty(HashMap.prototype, 'clone', {
	value: function(){
		var map = new HashMap();
		for(var key in this){
			map[key] = this[key];
		}
		map.size = this.size;
		
		return map;
	},
	writable: false, 
	enumerable: false,
	configurable: false
});

var map = new HashMap();

var obj = {hashCode: function(){return 'hashcode: ' + this.toString()}};
var obj2 = {};
map.put(obj, 'test');
map.put(obj2, 'test2');
map['ttt'] = 'test3';
console.log(map.size);