$(function(){
	$('.field_with_errors').closest('.control-group').addClass("error");
});


function remove_fields (link_tag) {
	// Set the hidden field value to true
	$(link_tag).prev("input[type=hidden]").val("1");
	// hide the current fields surrounded this link_tag
	$(link_tag).closest(".removable-fields").hide();
};

function add_fields(link_tag, association, content) {
	var id = new Date().getTime();
	var regexp = new RegExp("new_" + association, "g")
	
	$(link_tag).parent().before(content.replace(regexp, id));
};

function showMap(name, label, content, lat, lng) {
        $('#map-dialog > .modal-header > h3').html(name);
        // replace src with the
        $('#map-image-holder').attr("src", "http://maps.google.com/maps/api/staticmap?size=450x300&sensor=false&zoom=16&markers=" + lat + "%2C" + "" + lng); 
        $('#map-dialog > .modal-body > p').html("</br>"+ " <span class='label notice'>" + label +"</span> "+content);
        $('#map-dialog').modal('show')
};

$(document).ready(function(){
        $('#map-dialog').bind('hide', function () {
                
        })
        
        
});