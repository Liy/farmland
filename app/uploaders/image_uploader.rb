# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  if Rails.env.production?
    storage :fog
  else
    storage :file
  end
  
  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/images"
  end
  
  # Create different versions of your uploaded files:
  version :thumb do
    process :resize_to_limit => [300, 300]
  end
  
  # no larger than 2000*2000
  process :resize_to_limit => [2000, 2000]
  
  process :auto_rotate
  
  process :watermark => "#{Rails.root}/app/assets/images/watermark.png"
  
  
  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # support extra canon raw format
  def extension_white_list
      %w(jpg jpeg gif png raw)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
    # "#{File.basename(original_filename, '.*')}_#{Time.now.to_formatted_s(:number)}#{File.extname(original_filename)}" if original_filename
    "#{model.image_hash}#{File.extname(original_filename).downcase}" if original_filename
  end
  
  
  def auto_rotate
    manipulate! do |img|
      # have to open image first: https://github.com/probablycorey/mini_magick/issues/68
      image = MiniMagick::Image.open(img.path)
      img.auto_orient
      # not exactly sure why I need to yield and return the image
      # note that the doc example is NOT working: http://carrierwave.rubyforge.org/rdoc/classes/CarrierWave/MiniMagick.html
      # From the github issue list, seems like it is not maintained anymore:
      # https://github.com/jnicklas/carrierwave/issues/436
      img = yield(img) if block_given?
      img
    end
  end
  
  def quality(percentage)
    manipulate! do |img|
      img.percentage
      img = yield(img) if block_given?
      return img      
    end
  end
  
  def strip
    manipulate! do |img|
      img.strip
      img = yield(img) if block_given?
      return img      
    end
  end
  
  # https://gist.github.com/718055
  def watermark(file_path)
    manipulate! do |img|
      img = img.composite(MiniMagick::Image.open(file_path)) do |c|
        c.gravity 'SouthWest'
      end
    end
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :scale => [50, 50]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_white_list
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

end
