module ApplicationHelper
  def link_to_remove_fields(name, btnType, f)
    # this returns two strings, one is a hidden field and another is a link to the remove
    # field function
    return f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)", :class => "btn " + btnType)
  end
  
  # association should be a symbol
  def link_to_add_fields(name, f, association)
    # create corresponding ruby instance of the association so the field can reference to, e.g, if it is a address, then
    # FarmerEmail ruby object will be created.
    new_object = f.object.class.reflect_on_association(association).klass.new
    
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    
    link_to_function(name, "add_fields(this, '#{association}', '#{escape_javascript(fields)}')", :class => "btn btn-info btn-small")
  end
  
  def link_to_google_map (addr)
    	s = Geocoder.search( addr ) 
  		lat = s[0].latitude
  		lng = s[0].longitude
  end
  
  # bootstrap style of pagination
  # Based on https://gist.github.com/1205828, in turn based on https://gist.github.com/1182136
  class BootstrapLinkRenderer < ::WillPaginate::ActionView::LinkRenderer
    protected

    def html_container(html)
      tag :div, tag(:ul, html), container_attributes
    end

    def page_number(page)
      tag :li, link(page, page, :rel => rel_value(page)), :class => ('active' if page == current_page)
    end

    def gap
      tag :li, link(super, '#'), :class => 'disabled'
    end

    def previous_or_next_page(page, text, classname)
      tag :li, link(text, page || '#'), :class => [classname[0..3], classname, ('disabled' unless page)].join(' ')
    end
  end

  def bootstrap_will_paginate(pages)
    will_paginate(pages, :class => 'pagination', :inner_window => 2, :outer_window => 0, :renderer => BootstrapLinkRenderer, :previous_label => '&larr;'.html_safe, :next_label => '&rarr;'.html_safe)
  end
  

  
end
