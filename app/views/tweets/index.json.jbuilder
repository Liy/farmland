#coding: utf-8
json.tweets @tweets do |json, tweet|
  json.id tweet[:id]
  json.core_id tweet[:core_id]
  json.content tweet[:content]
end