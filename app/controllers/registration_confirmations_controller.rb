# TODO: there should be an daily task to remove the inactivated account which is too old(1 hour?)
class RegistrationConfirmationsController < ApplicationController
  layout 'greet'
    
  skip_authorization_check
  
  def edit    
    # the id is actually a random token
    @user = User.find_by_activation_token(params[:id])
    
    # if user clicked activation again...
    if @user.nil?
      redirect_to root_path, :flash => {:error => "This account has already been activated."}
      return
    end
    
    # we only need to check for the updated_at time, since its intial value is the same
    # as the created_at.
    if @user.updated_at < 10.minutes.ago
      # resend the confirmation again... so they can re-confirm
      @user.send_registration_confirmation
      
      flash[:error] = "This registration confirmation has expired. A new confirmation has issued. Check your email box."
      redirect_to new_registration_confirmation_path
    else
      # activate the account
      @user.activate
      
      # by default create staff role for the user
      @user.assign!(:staff)
      
      # It is always a good idea to ask user to provide credential to sign, just in case user
      # provide the wrong email address and email sent to other people...
      flash[:success] = "You account has been activated, you can sign in now. You are assigned as 'staff'"
      redirect_to signin_path
    end
  end
  
end
