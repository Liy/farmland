#coding: utf-8
class ApplicationController < ActionController::Base
  protect_from_forgery
  
  # ensure every action is authorized, lock it down
  check_authorization
  
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :flash => {:error => t('flash.unauthorized')}
  end
  
  before_filter :setup_locale
  
  def setup_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
  
  def site_name
    return t(:title)
  end
  helper_method :site_name
  
  def company_name
    return t(:company)
  end
  helper_method :company_name
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  # expose this method to other controllers
  helper_method :current_user
  
  
  
  # check to see if the request is from a local network range...
  def in_local_network?
    # TODO: check a certain range of ip address 
    # require 'ipaddr'
    #     ip_i = IPAddr.new(request.remote_ip).to_i
    #     return ip_i == IPAddr.new('127.0.0.1').to_i
    return true
  end
  helper_method :in_local_network?
end
