class ImagesController < ApplicationController
  load_and_authorize_resource
  
  def index
    @images = Image.search(params)
  end
  
  def show
    @image = Image.find(params[:id])
  end
  
  def new
    @image = Image.new
    render 'new', :layout => 'image'
  end

  def upload
    @image = Image.new(params[:image])
    @image.user_id = current_user.id
    
    # pre-process the image, extract exif information and genreate sha1
    # hash for file name. Note only call this method on image first time
    # is uploaded. Edit will not trigger this method.
    @image.pre_process
    
    if @image.save
      respond_to do |format|
         format.html {  
           render :json => [@image.to_jq_upload].to_json, 
           :content_type => 'text/html',
           :layout => false
         }
         format.json {  
           render :json => [@image.to_jq_upload].to_json			
         }
      end
    else
       render :json => [{:error => "upload failed"}], :status => 304
    end
    
  end
  
  def edit
    @image = Image.find(params[:id])
  end
  
  def update
    @image = Image.find(params[:id])
    
    if @image.update_attributes(params[:image])
      flash[:success] = 'Update sucessfully'
      redirect_to image_path(@image.id)
    else
      flash.now[:error] = 'Cannot update image'
      render 'edit'
    end
    
  end
  
  def destroy
    @image = Image.find(params[:id])
    @image.destroy
    
    flash["success"] = "Successfully deleted the image"
    redirect_to images_path
  end
end
