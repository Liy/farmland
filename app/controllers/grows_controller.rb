class GrowsController < ApplicationController
  load_and_authorize_resource
  
  # list the grows belongs to a specific farmer
  def index
     @grows = Grow.search(params)
  end
  
  # show the specific grow of a specific farmer
  def show
    @grow = Grow.find(params[:id])
  end
  
  # add a new grow to the farmer
  def new
    @farmer = Farmer.find_by_id(params[:farmer_id])
    @grow = @farmer.grows.build
    @crops = Crop.all
  end

  def create
    @farmer = Farmer.find_by_id(params[:farmer_id])
    @grow = @farmer.grows.build(params[:grow])
    if @grow.save
      flash[:success] = 'Successfully created the grow!'
      redirect_to farmer_path(@farmer)
    else
      render 'new', :flash => {:error => @grow.errors.full_messages}
    end
  end

  def edit

  end

  def update
    if @grow.update_attributes(params[:grow])
      flash[:success] = 'Successfully updated the grow!'
      redirect_to grow_path(@grow)
    else
      flash[:success] = 'Update failed!'
      render 'edit'
    end
  end

  def destroy
  end

end
