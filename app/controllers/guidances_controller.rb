class GuidancesController < ApplicationController
  load_and_authorize_resource :user
  load_and_authorize_resource :guidance, :through => :user 

   
  def all
    @guidances = Guidance.all
  end

  def index
    @user = User.find_by_id(params[:user_id])
    @guidances = @user.guidances
  end

  def show
    @guidances = Guidance.find_by_id(params[:id])
  end

  def new
    @user = User.find_by_id(params[:user_id])
    @guidance = @user.guidances.build
    @farmers = Farmer.all
  end

  def create
    @user = User.find_by_id(params[:user_id])
    @user.farmer_ids =  params[:selected_farmer_ids]
    
    redirect_to user_path(@user)
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
