class UsersController < ApplicationController
  # use greet layout on user signup
  layout 'greet', :only => [:new, :create]
  
  # this method will detect the restful style of request, load and authorize the
  # resrouce, so you don't need to query to the data anymore, it is already loaded.
  # load_and_authorize_resource
  
  skip_authorization_check :only => [:new, :create]
  
  
  def index
    authorize! :index, User
    
    @users = User.all
  end
  
  def show
    @user = User.find_by_id(params[:id])
    
    authorize! :index, @user
    
    render :layout => 'application_full'
  end

  def new
    # if user is in local network, they can register a new user
    if !in_local_network?
      flash[:error] = 'You are not in the local network, cannot create a new user!'
      redirect_to root_path
      return
    end
    
    @user = User.new
  end

  def create
    # if user is in local network, they can register a new user
    if !in_local_network?
      flash[:error] = 'You are not in the local network, cannot create a new user!'
      redirect_to root_path
      return
    end
    
    @user = User.new(params[:user])
    if @user.save
      # send out the activation email to the user
      @user.send_registration_confirmation
      # route user to the confirmation email sent page
      flash[:info] = "An confirmation email has sent to your mailbox"
      redirect_to new_registration_confirmation_path
    else
      flash[:error] = @user.errors.full_messages
      render 'new'
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end
  
end
