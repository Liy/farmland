#coding: utf-8
class HomeController < ApplicationController
  skip_authorization_check
  
  def index
    # get the rss information
    require 'simple-rss'
    require 'open-uri' 
    require 'iconv'
    
    # force to read rss as UTF-8
    i = Iconv.new('UTF-8', 'UTF-8')
    # Seems like there is a bug on bitbucket's rss feed. If you try to open the feed it will auto detect the 
    # encoding as western ISO-8859-1, but it is actually is in UTF-8 encoding
    @rss = SimpleRSS.parse i.iconv(open('https://bitbucket.org/Liy/farmland/rss?token=d6396f0515485dc58a8de43fe7328bc2').read)
    
    if current_user.nil?
      flash.now[:info] = "超级帐户：li_yang</br>领班用户名：liu_sisi<br/>普通员工用户名：zeng_ziyi<br/>密码：0000"
    	render 'signin', :layout => 'greet'
    else
      # for now redirect to current user information page.
      # redirect_to current_user
      redirect_to tweets_path
    end
  end
end
