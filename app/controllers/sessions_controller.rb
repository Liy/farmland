#coding: utf-8
class SessionsController < ApplicationController
  layout 'greet'
  
  skip_authorization_check
  
  def new
  end
  
  # authenticate process
  def create
    user = User.find_by_username(params[:username])
    
    # make sure user exist
    if user
      # check to see if account is activated, if not do not sign in
      if !user.activated?
        inactivated
        return
      end
      
      # do authentication
      if user.authenticate(params[:password]) # && verify_recaptcha
        auth_success user
      else
        auth_invalid user
      end
    else
      auth_invalid user
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_path, :flash => {:info => t('account.signout')}
  end
  
  private
  def inactivated
    flash.now[:error] = t('account.errors.inactivated')
    render 'new'
  end
  
  def auth_success(user)
    session[:user_id] = user.id
    redirect_to user_path(current_user), :flash => {:success => t('account.signin')}
  end
  
  def auth_invalid(user)
    flash.now[:error] = t('account.errors.auth_failed')
    render 'new'
  end
  
end
