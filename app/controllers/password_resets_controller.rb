class PasswordResetsController < ApplicationController
  skip_authorization_check
  
  # use greet as the layout
  layout 'greet'
  
  def new
    if !in_local_network?
      flash[:error] = 'You cannot reset password outside of local network!'
      redirect_to root_path
      return
    end
  end
  
  def create
    if verify_recaptcha
      user = User.find_by_email(params[:email])
      
      # this user is not activated yet!
      if user && !user.activated?
        flash[:error] = 'This account is not activated, please check your mailbox for confirmation email'
        redirect_to root_path
        return
      end
      
      user.send_password_reset if user
      redirect_to root_path, :flash => {:info => 'Password reset email sent.'}
    else
      flash[:recaptcha_error] = t('recaptcha.errors.incorrect_captcha')
      redirect_to new_password_reset_path
    end
  end
  
  def edit
    @user = User.find_by_password_reset_token(params[:id])
    
    if !@user
      redirect_to root_path, :flash => {:error => 'Invalid token.'}
      return
    end
    
    if @user.updated_at < 5.hours.ago
      redirect_to root_path, :flash => {:error => 'Password reset has expired.'}
    end
  end
  
  def update
    @user = User.find_by_password_reset_token!(params[:id])
    
    if @user.update_attributes(params[:user])  
      redirect_to signin_path, :flash => {:success => 'Password has been reset.'}
    else
      render 'edit', :flash => {:error => @user.errors.full_messages}
    end
  end
end
