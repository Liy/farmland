class VersionsController < ApplicationController
  load_and_authorize_resource
  
  def revert
    @version = Version.find(params[:id])
    # undo edit and destroy
    if @version.reify
      @version.reify.save!
    else   # undo creation
      @version.item.destroy
    end
    flash[:info] = "Undo #{@version.event}"
    redirect_to :back
  end
end
