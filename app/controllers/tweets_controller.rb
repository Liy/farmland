#coding: utf-8
class TweetsController < ApplicationController
  skip_authorization_check
    
  def index
    @user = User.find_by_id(params[:user_id])
    # render :text => @user.timeline
    if @user
      @tweets = @user.timeline
    end
  end
  
  def inbox
    # current user's inbox
    # render :text => current_user.inbox
    @tweets = current_user.inbox
  end
  
  def show
    # show specific user's timeline
  end
  
  def new
    # create a new tweet
    @tweet = Tweet.new
  end
  
  def reply
    
  end
  
  def create
    # 
    current_user.tweet(params[:content])
    redirect_to '/tweets', :flash => {:success => "发送成功"}
  end
  
  def retweet
    # retweet a tweet
  end
  
  def destroy
    
  end
end
