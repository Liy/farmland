class PesticidesController < ApplicationController
  load_and_authorize_resource
  
  def index
    @pesticides = Pesticide.search(params)
  end

  def show
  end

  def new
  end

  def create
    if @pesticide.save
      flash[:success] = "Successfully created farmer"
      redirect_to pesticide_path(@pesticide)
    else
      render 'new', :flash => {:error => @pesticide.errors.full_messages}
    end
  end

  def edit
  end

  def update
    if @pesticide.update_attributes(params[:pesticide])
      flash[:success] = 'Update sucessfully'
      redirect_to pesticide_path(@pesticide)
    else
      render 'edit'
    end
  end

  def destroy
    @pesticide.destroy
    flash[:success] = "Successfully removed pesticide: " + @pesticide.name
    redirect_to pesticides_path
  end
end
