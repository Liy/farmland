class FarmersController < ApplicationController
  load_and_authorize_resource
  
  def index
    @farmers = Farmer.search(params)
    # @farmers = Farmer.all
  end
  
  def show
    # @farmer = Farmer.find(params[:id])
    @grows = @farmer.grows    
  end
  
  def new
    # @farmer = Farmer.new
      1.times {
        telecom = @farmer.telecoms.build
      }
      
      1.times {
        email = @farmer.emails.build
      }
      
      1.times do
        address = @farmer.addresses.build
      end
  end

  def create
    # @farmer = Farmer.new(params[:farmer])
    if @farmer.save
      flash[:success] = "Successfully created farmer"
      redirect_to farmer_path(@farmer.id)
    else
      render 'new', :flash => {:error => @farmer.errors.full_messages}
    end
  end
  
  def edit
    # @farmer = Farmer.find(params[:id])
  end
  
  def update
    # @farmer = Farmer.find(params[:id])
      if @farmer.update_attributes(params[:farmer])
        flash[:success] = 'Update sucessfully'
        redirect_to farmer_path(@farmer.id)
      else
        render 'edit'
      end
  end
  
  def destroy
    # @farmer = Farmer.find(params[:id])
    @farmer.destroy
    flash[:success] = "Successfully removed farmer: " + @farmer.name
    redirect_to farmers_path
  end
end
