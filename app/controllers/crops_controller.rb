class CropsController < ApplicationController
  load_and_authorize_resource
  # skip_authorization_check
  
  # searching will trigger this method
  def index
    @crops = Crop.search(params)
    # @crops = Crop.all
  end

  def show
    @crop = Crop.find(params[:id])
  end

  def new
    @crop = Crop.new
  end

  def create
    @crop = Crop.new(params[:crop])
    if @crop.save
      redirect_to crop_path(@crop.id), :flash => {:success => "Successfully created crop"}
    else
      render 'new', :flash => {:error => @crop.errors.full_messages}
    end
  end

  def edit
  end

  def update
  end

  def destroy
    @crop = Crop.find(params[:id])
    @crop.destroy
    flash[:info] = "Successfully removed crop:#{@crop.category} #{undo_link}"
    redirect_to crops_path
  end
  
  private 
  
  def undo_link
    view_context.link_to('undo', revert_version_path(@crop.versions.scoped.last), :method => :post)  
  end
end
