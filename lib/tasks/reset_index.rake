namespace :tire do
  desc "Reset all indices"
  task :reset => :environment do
    Tire.index 'farmers' do
      delete
      import Farmer.all
    end
    
    Tire.index 'grows' do
      delete
      import Grow.all
    end
    
    Tire.index 'crops' do
      delete
      import Crop.all
    end
    
    Tire.index 'pesticides' do
      delete
      import Pesticide.all
    end
    
    Tire.index 'images' do
      delete
      import Image.all
    end
  end
end
