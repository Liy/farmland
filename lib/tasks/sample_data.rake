#coding: utf-8
namespace :db do
  desc "Fill database with sample data, and index them using elasticsearch"
  task :populate => :environment do
    Rake::Task['db:reset'].invoke
    
    # Remove all keys from Redis current database
    REDIS.flushdb
    
    # once the task is finished, the env variable will be auto removed.(this env variable only exsit
    # in this rake process)
    ENV["seed"] = 'seeding'
    
    create_roles
    make_users
    make_assignments
    
    make_farmers
    make_guidances
    make_crops
    make_pesticide
    
    create_harms
    create_fertilizers
    
    create_grows
    
    make_tweets
    
    # remove all the previous index and import new index
    # this task does not use ENV["seed"] as index condition
    Rake::Task['tire:reset'].invoke    
  end
end

def create_roles
  Role.create!(:name => "manager")
  Role.create!(:name => "supervisor")
  Role.create!(:name => "staff")
end

def make_assignments
  @li_yang.assign!(:manager)
  @liu_sisi.assign!(:supervisor)
  @zeng_ziyi.assign!(:staff)
end

def make_users
  @li_yang = User.create(:username => 'li_yang', :name => '李旸', :email => 'li_yang@ac.com', :password => '0000', :mobile => '123456')
  @dong_chao = User.create(:username => 'dong_chao', :name => '董超', :email => 'dong_chao@ac.com', :password => '0000', :mobile => '123456')
  @rokey = User.create(:username => 'rokey', :name => 'Rokey', :email => 'rokey@ac.com', :password => '0000', :mobile => '123456')
  @liu_sisi = User.create(:username => 'liu_sisi', :name => '刘思斯', :email => 'liu_sisi@ac.com', :password => '0000', :mobile => '123456')
  @pampy = User.create(:username => 'pampy', :name => 'Pampy', :email => 'pampy@ac.com', :password => '0000', :mobile => '123456')
  @zeng_ziyi = User.create(:username => 'zeng_ziyi', :name => '曾芓伊', :email => 'zeng_ziyi@ac.com', :password => '0000', :mobile => '123456')
  @li_dan = User.create(:username => 'li_dan', :name => '李丹', :email => 'li_dan@ac.com', :password => '0000', :mobile => '123456')
  @zhang_yanlong = User.create(:username => 'zhang_yanlong', :name => '张彦龙', :email => 'zhang_yanlong@ac.com', :password => '0000', :mobile => '123456')
  @li_kaifu = User.create(:username => 'li_kaifu', :name => '李开复', :email => 'li_kaifu@ac.com', :password => '0000', :mobile => '123456')
  @fang_datong = User.create(:username => 'fang_datong', :name => '方大同', :email => 'fang_datong@ac.com', :password => '0000', :mobile => '123456')
  @ren_zhiqiang = User.create(:username => 'ren_zhiqiang', :name => '任志强', :email => 'ren_zhiqiang@ac.com', :password => '0000', :mobile => '123456')
  @shu_qi = User.create(:username => 'shu_qi', :name => '舒淇', :email => 'shu_qi@ac.com', :password => '0000', :mobile => '123456')
  @kevin = User.create(:username => 'kevin', :name => 'Kevin', :email => 'kevin@ac.com', :password => '0000', :mobile => '123456')
end

def make_farmers
  phoneTypes = ["座机","手机","传真"]
  addressTypes = ["家庭","农场","工作"]
  emailTypes = ["个人","工作"]
  
  100.times do |n|
    area = rand(100)+10
    farmer = Farmer.new(:name => Faker::Name.name,
      :arable => rand(100)+10,
      :cultivated => area - rand(area/3))
    
    samples = addressTypes.sample( rand(addressTypes.count+1) )
    samples = [samples] if !samples.respond_to?(:each)
    samples.each do |type|    
      FarmerAddress.create!(:label => type,
              :detail => Faker::Address.street_address,
              :city => Faker::Address.city,
              :province => Faker::Address.state,
              :country_region => Faker::Address.country,
              :postcode => Faker::Address.postcode,
              :farmer_id => n
      )
    end
    
    samples = emailTypes.sample( rand(emailTypes.count+1) )
    samples = [samples] if !samples.respond_to?(:each)
    samples.each do |type|
      FarmerEmail.create!(:label => type,
        :value => Faker::Internet.email,
        :farmer_id => n
      )
    end
    
    samples = phoneTypes.sample( rand(phoneTypes.count+1) )
    samples = [samples] if !samples.respond_to?(:each)
    samples.each do |type|
      FarmerTelecom.create!(:label => type,
        :value => Faker::PhoneNumber.phone_number,
        :farmer_id => n
      )
    end
    
    farmer.save
  end
end

def make_guidances
  users = User.all
  farmers = Farmer.all
    
  userSamples = users.sample(rand(users.count))
  userSamples = [userSamples] if !userSamples.respond_to?(:each)
  userSamples.each do |user|
    
    farmerSamples = farmers.sample( rand(26) )
    farmerSamples = [farmerSamples] if !farmerSamples.respond_to?(:each)
    farmerSamples.each do |farmer|
      user.support!(farmer, Faker::Lorem.sentences(4)) 
    end
  end
end

def make_crops
  Crop.create!(:category => "苹果",
    :variety => "红富士",
    :quality => "First class",
    :growth_start => Date.new(2011,2,17),
    :growth_end => Date.new(2011,9,1),
    :image => "http://upload.wikimedia.org/wikipedia/commons/c/c0/Rosaceae_Malus_pumila_Malus_pumila_Var_domestica_Apples_Fuji.jpg"
  )
  
  Crop.create!(:category => "柑橘",
    :variety => "雷波脐橙",
    :quality => "First class",
    :growth_start => Date.new(2011,3,17),
    :growth_end => Date.new(2011,11,1),
    :image => "http://upload.wikimedia.org/wikipedia/commons/b/b0/OrangeBloss_wb.jpg"
  )
  
  Crop.create(
    :category => "水稻",
    :variety => "籼型三系杂交糯稻",
    :growth_start => Date.new(2011,5,17),
    :growth_end => Date.new(2011,8,1),
    :image => "http://www.sclongping.com.cn/UploadFiles/2009922482874.jpg"
  )
  
  Crop.create(
    :category => "小麦",
    :variety => "新麦26",
    :growth_start => Date.new(2011,4,27),
    :growth_end => Date.new(2011,10,29),
    :image => "http://www.nongkoo.com/UpFile/Product/201112/1112261110216657.jpg"
  )
  
  Crop.create(
    :category => "向日葵",
    :variety => "Teddybear Golden Yellow",
    :growth_start => Date.new(2011,6,7),
    :growth_end => Date.new(2011,9,2),
    :image => "http://1.bp.blogspot.com/_7pEUAD6CGo8/TPWO9mBrALI/AAAAAAAAH1U/mh5b0HO0zpk/s1600/Teddy%2BBear%2BSunflower.jpg"
  )
  
  Crop.create(
    :category => "油菜",
    :variety => "蓉油3号",
    :growth_start => Date.new(2011,6,7),
    :growth_end => Date.new(2011,9,2),
    :image => "http://pic4.nipic.com/20090906/842488_150636038844_2.jpg"
    
  )
  
  Crop.create(
    :category => "萝卜",
    :variety => "水果萝卜",
    :quality => "二级",
    :growth_start => Date.new(2011,6,7),
    :growth_end => Date.new(2011,9,2),
    :image => "http://images.99114.com/upfile/pro/20080108/1517006571.jpg"
  )
  
  Crop.create(
    :category => "芹菜",
    :variety => "SG黄嫩西芹",
    :quality => "一级",
    :growth_start => Date.new(2011,10,17),
    :growth_end => Date.new(2011,4,22),
    :image => "http://www.hongchengqc.com/images/pt/1_d.jpg"
  )
  
  Crop.create(
    :category => "白菜",
    :variety => "胶州白菜",
    :quality => "一级",
    :growth_start => Date.new(2011,6,27),
    :growth_end => Date.new(2011,9,5),
    :image => "http://a0.att.hudong.com/66/25/01300000164646121324258496153_s.jpg"
  )
  
  Crop.create(
    :category => "土豆",
    :variety => "紫薯",
    :quality => "First class",
    :growth_start => Date.new(2011,4,22),
    :growth_end => Date.new(2011,6,25),
    :image => "http://a0.att.hudong.com/66/25/01300000164646121324258496153_s.jpg"
  )
  
  Crop.create(
    :category => "李子",
    :variety => "黑宝石",
    :quality => "First class",
    :growth_start => Date.new(2011,6,22),
    :growth_end => Date.new(2011,7,15),
    :image => "http://www.foodqs.cn/memberpicture/sonney20097420549.jpg"
  )
  
  Crop.create(
    :category => "草莓",
    :variety => "Kletter-Erdbeere, Hummi HZ",
    :quality => "First class",
    :growth_start => Date.new(2011,7,9),
    :growth_end => Date.new(2011,8,30),
    :image => "http://pic.incards.cn/upimg/news/1227517676517358.jpg"
  )
  
  Crop.create(
    :category => "樱桃",
    :variety => "旱大果",
    :quality => "First class",
    :growth_start => Date.new(2011,7,9),
    :growth_end => Date.new(2011,8,30),
    :image => "http://blog.zikao365.com/Baileys/wp-content/blogs.dir/5181/photos/5181_1276136265_33340.jpg"
    
  )
  
  Crop.create(
    :category => "当归",
    :growth_start => Date.new(2011,12,9),
    :growth_end => Date.new(2011,2,10)
  )
  
  Crop.create(
    :category => "金银花",
    :growth_start => Date.new(2011,1,19),
    :growth_end => Date.new(2011,3,20),
    :image => "http://www.czjyh.com/czjyh.com/admin/upload/201114192521.jpg"
  )
end

def make_pesticide
  @pesticide_names = ["康宽","普尊","农得时","灭蝇胺","凯恩","升势","诚诺1号","玉家乐","草崩","福万甲","安泰生","吡虫啉","退蚜","百穿"]
  categories = ["杀虫剂","杀菌剂","除草剂"]
  
  @pesticide_names.each do |name|
    Pesticide.create(
      :name => name,
      :category => categories.sample(1)[0]
    )
  end
end

def create_fertilizers
  type = ["有机","复合"]
  20.times do
    Fertilizer.create(
      :name => "肥料名字#{Faker::Internet.user_name}",
      :category => type.sample(1)[0],
      :n => rand(5) + 1,
      :p => rand(5) + 1,
      :k => rand(5) + 1,
      :others => Faker::Lorem.words,
    )
  end
end

def apply_fertilizers(grow)
  rand(5).times do
    fertilizerSamples = Fertilizer.all.sample( rand(Fertilizer.all.count) + 1 )
    fertilizerSamples = [fertilizerSamples] if !fertilizerSamples.respond_to?(:each)
    fertilizerSamples.each do |fertilizer|
      # choose random fertilizers to apply to the grow
      grow.fertilize!(fertilizer, rand(10), Time.at(rand * Time.now.to_i).to_date)
    end
  end
end

def create_grows
  farmers = Farmer.all
  crops = Crop.all
  
  farmerSamples = farmers.sample( rand(farmers.count+1) )
  farmerSamples = [farmerSamples] if !farmerSamples.respond_to?(:each)
  farmerSamples.each do |farmer|
    
    cropSamples = crops.sample( rand(crops.count+1) )
    cropSamples = [cropSamples] if !cropSamples.respond_to?(:each)
    cropSamples.each do |crop|
      grow = Grow.new(
        :sown_area => rand(100)+10,
        :sowing_date => Time.at(rand * Time.now.to_i).to_date,
        :crop_id => crop.id,
      )  
      # farmer_id is protected from mass-assignment
      grow.farmer_id = farmer.id
      if rand(10) > 5
        grow.note = Faker::Lorem.sentences(4)
      end
      grow.save
      
      apply_fertilizers(grow)
      apply_pesticides(grow)
      suffer_harms(grow)
      
      # add harvest information
      if rand(10) > 5
        harvest(grow.id)
      end
    end
  end
end

def apply_pesticides(grow)
  rand(5).times do
    pesticideSamples = Pesticide.all.sample( rand(Pesticide.all.count) + 1 )
    pesticideSamples = [pesticideSamples] if !pesticideSamples.respond_to?(:each)
    pesticideSamples.each do |pesticide|
      # choose random pesticide to apply to the grow
      grow.use_pesticide!(pesticide, rand(10), Time.at(rand * Time.now.to_i).to_date)
    end
  end
end

def create_harms
  things = ["蝗虫", "冰雹", "霜冻", "蚜虫", "稻瘟病", "白粉病", "褐纹病", "炭疽病", 
    "黑腐病", "黄叶病", "早疫病", "蓟马", "条斑病", "稻曲病", "麻斑病", "斑枯病", "螟虫", "白绢病",
    "花叶病", "红蜘蛛", "茶黄螨", "褐色腐败病", "卷叶病", "颖枯病", "黑粉病", "霜霉病", "叶锈病", "角斑病"]
    
  things.each do |n|
    Harm.create(:name => n)
  end
end

def suffer_harms(grow)
  harms = Harm.all
      
  harmSamples = harms.sample( rand(6) )
  harmSamples = [thingSamples] if !harmSamples.respond_to?(:each)
  harmSamples.each do |h|
    grow.suffer!(h)
  end
end

def harvest ( grow_id )
  HarvestInfo.create(
    :date => Time.at(Time.now.to_i*2).to_date,
    :yield => rand(3000) + 100,
    :price  => rand(10) + 0.5,
    :grow_id => grow_id
  )
end


def make_tweets
  REDIS.flushdb

  @li_yang.friend!( @liu_sisi )
  @li_yang.friend!( @zhang_yanlong )
  @li_yang.friend!( @zeng_ziyi )
  @li_yang.friend!( @dong_chao )
  @li_yang.follow!( @rokey )
  @li_yang.follow!( @li_kaifu )
  
  @liu_sisi.friend!( @zhang_yanlong )
  @liu_sisi.friend!( @zeng_ziyi )
  @liu_sisi.friend!( @pampy )
  
  @zeng_ziyi.friend!( @li_dan )
  @zeng_ziyi.follow!( @fang_datong )
  @zeng_ziyi.follow!( @ren_zhiqiang )
  
  @zhang_yanlong.friend!( @kevin )
  @zhang_yanlong.follow!( @li_kaifu )
  
  @li_kaifu.friend!( @ren_zhiqiang )
  
  @fang_datong.follow!( @shu_qi )
  
  @ren_zhiqiang.follow!( @shu_qi )
  
  
  
  retweet_id = @ren_zhiqiang.tweet("取消各种行政干预，还市场之自由。")
  @li_kaifu.retweet(retweet_id)
  @zeng_ziyi.retweet(retweet_id)
  
  id = @li_yang.tweet("明天要上班了，不爽！")
  reply_id = @liu_sisi.reply(id, "我明天给你弄蛋糕嘛！", true)
  @zeng_ziyi.reply(reply_id, "我也要吃!", false)
  @pampy.reply(reply_id, "秀幸福哇？！", false)
  @li_yang.reply(reply_id, "我要吃泡芙", false)
  
  
  # 刘思斯 retweet 曾芓伊's retweet
  @liu_sisi.retweet(retweet_id)
end