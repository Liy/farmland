# unicorn server config
# 4 forked processes from the master process, number of unicorn workers to spin up
worker_processes 4
# restart worker which hangs for 120 seconds
timeout 120

# Specifically for new relic...
# https://newrelic.com/docs/troubleshooting/im-using-unicorn-and-i-dont-see-any-data
preload_app true

# I keep getting 'ActiveRecord::StatementInvalid ...' Not sure why.
# but will give these code a try... might work... then it will be a magic!
after_fork do |server, worker| 
  defined?(ActiveRecord::Base) and 
  ActiveRecord::Base.establish_connection 
end