# connect to the elasticsearch server
Tire.configure do
  logger STDERR
  
  # you need to specify the uri like:
  # http://localhost:9200
  # if you want to test it on local machine
  url ENV['ELASTIC_SEARCH_URI']
end