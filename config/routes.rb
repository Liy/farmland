AukingInformationDataSystem::Application.routes.draw do

  # account activation
  resources :registration_confirmations, :only => [:new, :edit]
  # password reset
  resources :password_resets
  
  resources :users, :shallow => true do
    resources :guidances
  end  

  # tweets
  match 'tweets', :to => 'tweets#inbox'
  match 'tweets/new', :to => 'tweets#new'
  match 'tweets/create', :to => 'tweets#create'
  resources :users do
    resources :tweets
  end
  
  # versioning
  post "versions/:id/revert" => 'versions#revert', :as => 'revert_version'
  
  
  match '/guidances', :to => 'guidances#all'

  # session
  resources :sessions
  
  # also we allow admin to list all the grows of all the farmers
  match '/grows', :to => 'grows#index'
  resources :farmers, :shallow => true do
    # farmers has many crop grows
    resources :grows
  end
  
  resources :crops
  
  # :as => 'signin' ensures we have functions: path_to_signin, url_to_signin available
  match '/signin', :to => 'sessions#new'
  match '/signout', :to => 'sessions#destroy'
  match '/signup', :to => 'users#new'
  
  root :to => "home#index"
  
  # pesticides
  resources :pesticides
  
  # images
  resources :images
  match 'images/upload', :to => 'images#upload'


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
