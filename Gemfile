source 'http://rubygems.org'

gem 'rails', '3.2.2'

# Bundle edge Rails instead:
# gem 'rails',     :git => 'git://github.com/rails/rails.git'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
# I use less and pure javascript only
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier', '>= 1.0.3'
end

gem 'jquery-rails'

# Use sqlite3 during development and testing.
# But tell Heroku to use PostgreSQL in production deployment.
group :production do
  gem 'pg'
end

# Testing utils, we use rspec instead of rails' default test.
group :development, :test do
  gem 'sqlite3'

  gem 'rspec-rails'
  gem 'webrat'

  gem 'guard-rspec'
  gem 'guard-bundler'

  gem 'rb-fsevent'
  gem 'growl'
  
  gem 'debugger'
  
  # clean up the database after test
  gem 'database_cleaner'
  
  # Create object
  gem 'factory_girl_rails'
end

group :test do
  gem 'spork'
  gem 'guard-spork'
end

# To use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.0.0'

# Util to create fake stuff
# for server testing reason, I still left the faker gem here for depolyment
gem 'faker'
# group :development do
#	gem 'faker'
# end

gem 'geocoder'

# file and image unploader related gems
gem 'mini_magick'
gem 'carrierwave'
gem 'fog'

# undo redo
gem 'paper_trail'

# ruby api and dsl for elasticsearch
gem 'tire'

# pagination
gem 'will_paginate', '>3.0'

# recaptch
gem "recaptcha", :require => "recaptcha/rails"

# authorization
gem "cancan"

# for microblog
gem 'redis', :git => 'https://github.com/NoICE/redis-rb.git'

# client side validation for form input
gem 'client_side_validations'

# new relic rails application monitoring tool
gem 'newrelic_rpm'

# Use unicorn as the web server
gem 'unicorn'

# twitter-bootstrap-rails
# for now... keep it here since heroku will still use it to compile assets
gem 'twitter-bootstrap-rails', :git => 'http://github.com/seyhunak/twitter-bootstrap-rails.git'

# during the development, I'll display the update infor on the app front page
gem 'simple-rss'

# form
gem 'simple_form'

gem 'jbuilder'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'ruby-debug19', :require => 'ruby-debug'