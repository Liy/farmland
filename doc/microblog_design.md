#微博系统
该微薄系统为新浪weibo和twitter混合体。

##timeline
每人都有一个timeline。储存只有自己的微博和自己转发的微博。其他用户如果点击该用户的主页链接，就可以查看该用户的timeline。

用户所有的动作都会储存到自己的timeline下，包括所有类型的回复。转发也一样。

##inbox
每个人都有一个inbox。inbox包含timeline，timeline是inbox的子集。

具体tweet添加规则，下边不同类型的tweet会有介绍。

##tweets(微博)
* 所有关注该用户的人，都可以在各自的inbox内查看到该用户的tweets。
* 未关注该用户的人，可以在该用户的主页查看其tweets。

##@mentions（@提及）
* 提及某人。 例如：“@liy 在开发一个垃圾系统。”这跟twitter不一样，在twitter里边，如果微博以“@某某某”开头，那么该微博会被视为一个回复。但是@符号在该系统内，只有“提及”的意思，不会附带“回复”意义。
* 被@的人，会收到`提及通知`。

##retweet（转发）
转发相对简单。不允许转发人对原始信息进行添加，和twitter保持一致，区别于新浪weibo转发人可以添加文字。

* 转发，保持原始信息内容，不做添加。只会添加自动生成的标签“某某某转发”。
* 转发的微博会出现在转发者的所有好友的`inbox`内。
* 转发者会被添加到原始信息的`转发列表`中。

##reply（回复）
每条tweet都会有一个对应的`回复列表`，所有的对该tweet的回复都会储存在列表里边。其他具体情况，列表如下：
###回复原始信息
原信息作者通过`inbox`查看回复，并且有`回复通知`。其他用户可以展开原始信息通过`回复列表`查看。

* 该回复会出现在原信息作者的`inbox`内，并以“特殊”表现形式（显示 *回复@某某某* 标签）出现。
* 该回复会出现在原始信息`回复列表`内。无特殊形式。
* 原始信息的作者会收到`回复通知`。

###在回复列表下，回复某一回复
如果你主要是想与**原信息的回复者**交流。

被回复者能够通过自己的`inbox`查看到回复。原始信息作者只收到`回复通知`。原始信息作者和其他用户可以展开原始信息通过`回复列表`查看。

* 该回复会出现在被回复作者的`inbox`内，并以“特殊”表现形式（显示 *回复@某某某* 标签）出现。
* 该回复会出现在原始信息`回复列表`内。并以“特殊”表现形式（显示 *回复@某某某* 标签）出现。
* 原始信息的作者会收到`回复通知`。

###回复并强制公开
当你觉得自己的回复不但会对被回复者，并且会对其他人又帮助的时候，你可以采用回复并且强制公开的方式。

回复者的所有好友可以通过他们的`inbox`查看到回复。
###回复强制公开信息
…

…

---
##Redis data keys

###identifier
tweet:next_id => the next id for `tweet_id`

core:next_id => the next id for `core_id`

---
###Tweet
tweet:<`tweet_id`> => { id => `tweet_id`, core_id => `core_id`, pub_date => datetime }

tweet:<`tweet_id`>:retweeted_by => `user_id`

---
###Core, substance of the Tweet
core:<`core_id`> => { id => `core_id`, author => `user_id`, content => string, create_date => datetime }

core:<`core_id`>:attachment => { content => string, type => string }

core:<`core_id`>:replies => List of `tweet_id`

core:<`core_id`>:retweets => Set of `user_id`

core:<`core_id`>:favourites => Set of `user_id`

core:<`core_id`>:parents => List of parent `tweet_id`, ordered from root to direct parent.

---
###User
user:<`user_id`>:tweet:<`tweet_id`>:tags => Set of `tag_id`

user:<`user_id`>:favourites => Set of `tweet_id`

user:<`user_id`>:inbox => List of `tweet_id`

user:<`user_id`>:timeline => SortedSet of `tweet_id`

user:<`user_id`>:mentions => List of `tweet_id`

user:<`user_id`>:followers => Set of `user_id`

user:<`user_id`>:following => Set of `user_id`

---
###Channel
channel:<`channel_id`>:subscribers => Set of `user_id`
channel:<`channel_id`>:timeline => List of `tweet_id`