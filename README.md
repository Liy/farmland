奥坤 Farmland
============
测试网站地址: [Farmland](http://farmer.aircapsule.com "Farmland")

如何学习
---
网站完全基于Ruby on Rails框架，很多时候简称为Rails。需要注意的是，Ruby是一种编程语言，而Ruby on Rails只是一个用Ruby语言写的网页框架。网上也有其他Ruby写的框架，比如[Sinatra](www.sinatrarb.com)和[Merb](http://www.merbivore.com/)...

这里有一个比较靠谱的中文介绍：[如何从 0 开始学 ruby on rails](http://readful.com/post/12322300571/0-ruby-on-rails)。
需要指出的是，对于里边所谓的Rails不向下兼容是错误的。Rails遵循[Semantic Versioning](http://semver.org/)。

###### Semantic versioning

简单来说：

1. 版本号格式为X.Y.Z。分别为 major version，minor version，patch version。
2. 在保证向下兼容的情况下，修复了一些bugs，那么patch version会在现在的基础上增加1。
3. 在保证向下兼容的情况下，增添了一些功能，那么minor version会在现在的基础上增加1。
4. 当程序出现了不能向下兼容的修改或者功能添加，那么major version会在现在的基础上增加1。

所以说Rails不向下兼容是不完全正确的。

其余的信息都算比较准确，完全可以按照上边所介绍的学习Ruby和Ruby on Rails。

版本控制Git
-------
上边的中文介绍里边已经提供了相关的教程资源，git虽然上手比较困难，不过你只需要知道一些常用的命令就可以了。git在这个工程里边是必要的工具之一。Heroku（下边我会自习介绍）会用git作为的部署命令，相当方便。

如果你使用的Mac OS X，那么你的系统里边可能已经装有git。Windows的话，你需要自己安装咯。

……

结构组成介绍
----------

1. [reCaptcha](http://www.google.com/recaptcha) 验证码服务。
2. [New Relic](http://newrelic.com/) 程序监管。
3. [ImageMagick](http://www.imagemagick.org/script/index.php) 图片处理，提取EXIF信息
4. 采用 [Amazon Simple Storage Service (Amazon S3)](http://aws.amazon.com/s3/) 作为储存的图库。
5. 运行于 [Amazon Elastic Compute Cloud (Amazon EC2)](http://aws.amazon.com/ec2/) 上的ElasticSearch服务，用于全文搜索。
6. [Redis](http://redis.io/) 作为 Session 数据和拼音转换数据储存。(研究当中)
7. [node.js](http://nodejs.org/) 构建通知服务，部署在 [nodester](http://nodester.com/) (研究当中)
8. [Unicorn](http://unicorn.bogomips.org/) http web server
9. [Heroku](http://www.heroku.com/) 作为 Rails 部署服务器。

####下一版本
1. 支持移动设备
2. 开放API

……


环境搭建
--------------

###### 环境变量 ######

……


###### Heroku ######
现阶段，工程部署在[Heroku](http://www.heroku.com/)。Heroku提供了非常方便的部署过程。

1. 首先（当然，注册帐号应该是第一步）你需要安装一个叫[Heroku toolbelt](https://toolbelt.herokuapp.com/osx)的东西，它提供了一些command line下的命令。
2. 用heroku login，

……


For git setup and ssh key, please refers to:
http://help.github.com/mac-set-up-git/  	Mac OSX
http://help.github.com/win-set-up-git/		Windows
Although above instructions are for Github only(a git repository provider like Assembla, but does not offer free private hosting and svn hosting). But still useful when you try to generate ssh key for the first time. Only difference is, at the last step, "adding SSH key to github". you should copy paste your public key to Assembla:
https://www.assembla.com/user/edit/edit_git_settings


During the development, we will use Heroku as a temporary host. It is a Ruby on Rail host provider. By using git, we can very easily deploy our application onto their server(It's free as well!). The link below shows how to deploy to Heroku:
http://ruby.railstutorial.org/ruby-on-rails-tutorial-book#sec:deploying

Since Heroku does not support sqlite3(which is used by RoR by default), you may encounter some error messages asking you to install PostgreSQL adapter. Detailed information can be found in "Database Configuration" section of this page:
http://railsapps.github.com/rails-heroku-tutorial.html
Basically just telling Heroku to use PostgreSQL in production mode.