#coding: utf-8
FactoryGirl.define do
  # User role
  factory :role do
    name "manager"
  end
  
  
  # User
  factory :user do |f|
    sequence(:username) { |n| 
      puts "username: test_user#{n}"
      "testUser#{n}" 
    }
    password '0000'
    email { "#{username}@aircapsule.com" }
    name '某某某'
    mobile "07723452738"
  end
end