#coding: utf-8
require 'spec_helper'

describe User do
  
  before(:all) do
    @li_yang = FactoryGirl.create(:user, name:"李旸")
    @dong_chao = FactoryGirl.create(:user, name:"董超")
    @rokey = FactoryGirl.create(:user, name:"Rokey")
    @liu_sisi = FactoryGirl.create(:user, name:"刘思斯")
    @pampy = FactoryGirl.create(:user, name:"Pampy")
    @zeng_ziyi = FactoryGirl.create(:user, name:"曾芓伊")
    @li_dan = FactoryGirl.create(:user, name:"李丹")
    @zhang_yanlong = FactoryGirl.create(:user, name:"张彦龙")
    @li_kaifu = FactoryGirl.create(:user, name:"李开复")
    @fang_datong = FactoryGirl.create(:user, name:"方大同")
    @ren_zhiqiang = FactoryGirl.create(:user, name:"任志强")
    @shu_qi = FactoryGirl.create(:user, name:"舒淇")
    @kevin = FactoryGirl.create(:user, name:"Kevin")
  end
  
  before(:each) do    
    REDIS.flushdb

    @li_yang.friend!( @liu_sisi )
    @li_yang.friend!( @zhang_yanlong )
    @li_yang.friend!( @zeng_ziyi )
    @li_yang.friend!( @dong_chao )
    @li_yang.follow!( @rokey )
    @li_yang.follow!( @li_kaifu )
    
    @liu_sisi.friend!( @zhang_yanlong )
    @liu_sisi.friend!( @zeng_ziyi )
    @liu_sisi.friend!( @pampy )
    
    @zeng_ziyi.friend!( @li_dan )
    @zeng_ziyi.follow!( @fang_datong )
    @zeng_ziyi.follow!( @ren_zhiqiang )
    
    @zhang_yanlong.friend!( @kevin )
    @zhang_yanlong.follow!( @li_kaifu )
    
    @li_kaifu.friend!( @ren_zhiqiang )
    
    @fang_datong.follow!( @shu_qi )
    
    @ren_zhiqiang.follow!( @shu_qi )
    
    
    
    retweet_id = @ren_zhiqiang.tweet("取消各种行政干预，还市场之自由。")
    @li_kaifu.retweet(retweet_id)
    @zeng_ziyi.retweet(retweet_id)
    
    id = @li_yang.tweet("明天要上班了，不爽！")
    reply_id = @liu_sisi.reply(id, "我明天给你弄蛋糕嘛！", true)
    @zeng_ziyi.reply(reply_id, "我也要吃!", false)
    @pampy.reply(reply_id, "秀幸福哇？！", false)
    @li_yang.reply(reply_id, "我要吃泡芙", false)
    
    # 刘思斯 retweet 曾芓伊's retweet
    @liu_sisi.retweet(retweet_id)
  end
  
  it "李旸的粉丝" do
    @li_yang.followers_count.should eq(4)
  end
  
  it "李旸的inbox微博" do
    @li_yang.inbox.count.should eq(7)
    
    name = @li_yang.inbox[0][:retweeted_by]
    name.should match "#{@liu_sisi.name}"
  end
  
  it "任志强inbox微博" do
    @ren_zhiqiang.inbox.count.should eq(2)
  end
  
  it "曾芓伊inbox微博" do
    @zeng_ziyi.inbox.count.should eq(5)
    @zeng_ziyi.inbox[0][:id].to_i.should eq(9)
    @zeng_ziyi.inbox[0][:content].should match "取消各种行政干预，还市场之自由。"
  end
  
  it "刘思斯inbox微博" do
    @liu_sisi.inbox.count.should eq(6)
  end
  
  it "replies test" do
    
  end
  
end
