

# Signin signup in order to do the test.
def signupin
  role = FactoryGirl.create(:role)
  @current_user = FactoryGirl.create(:user)
  @current_user.role = role
  session[:user_id] = @current_user.id
  
  puts "user: #{@current_user.username} role: #{@current_user.roles}\n"
end
